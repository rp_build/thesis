> LTF.MX_bW.fun <- function(x) {coef(LTF.MX_bW.fit)[1]*x^0.65}
> coef(LTF.MX_bW.fit)
        A 
0.1664066 
> cor.test(predict(LTF.MX_bW.fit),ButanolNaarWater$KlA[ButanolNaarWater$mixer == 'LTF-MX'])

	Pearson's product-moment correlation

data:  predict(LTF.MX_bW.fit) and ButanolNaarWater$KlA[ButanolNaarWater$mixer == "LTF-MX"]
t = 15.4889, df = 4, p-value = 0.0001014
alternative hypothesis: true correlation is not equal to 0
95 percent confidence interval:
 0.9235554 0.9991404
sample estimates:
      cor 
0.9917662 

> LTF.VS_bW.fun <- function(x) {coef(LTF.VS_bW.fit)[1]*x^0.65}
> coef(LTF.VS_bW.fit)
         A 
0.06901668 
> cor.test(predict(LTF.VS_bW.fit),ButanolNaarWater$KlA[ButanolNaarWater$mixer == 'LTF-VS'])

	Pearson's product-moment correlation

data:  predict(LTF.VS_bW.fit) and ButanolNaarWater$KlA[ButanolNaarWater$mixer == "LTF-VS"]
t = 35.0704, df = 4, p-value = 3.945e-06
alternative hypothesis: true correlation is not equal to 0
95 percent confidence interval:
 0.9845136 0.9998311
sample estimates:
      cor 
0.9983779 

> Potmixer_bW.fun <- function(x) {coef(Potmixer_bW.fit)[1]*x^0.65}
> coef(Potmixer_bW.fit)
         A 
0.09655688 
> cor.test(predict(Potmixer_bW.fit),ButanolNaarWater$KlA[ButanolNaarWater$mixer == 'Potmixer'])

	Pearson's product-moment correlation

data:  predict(Potmixer_bW.fit) and ButanolNaarWater$KlA[ButanolNaarWater$mixer == "Potmixer"]
t = 25.2329, df = 4, p-value = 1.465e-05
alternative hypothesis: true correlation is not equal to 0
95 percent confidence interval:
 0.9703428 0.9996743
sample estimates:
      cor 
0.9968735 
