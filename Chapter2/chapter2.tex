\chapter{Reactor prototyping: Used fabrication techniques}
\label{ch: material and methods}
\vfill

%\begin{flushright}

In this chapter several fabrication techniques and materials employed for the
customized reactors are introduced. From these techniques laser ablation was
evaluated with its problems and advantages. Next, the main fabrication technique
for this thesis is evaluated -- CNC micro-milling. Some critical considerations
and suggestions are highlighted towards future fabrication. These techniques are
then used in \ref{ch:chapter3}, \ref{ch:chapter4} and \ref{ch:chapter5} for
reactor development.

%\end{flushright}
\pagebreak

\ifpdf
 \graphicspath{{Chapter2/Figs/Raster/}{Chapter2/Figs/PDF/}{Chapter2/Figs/}}
\else
 \graphicspath{{Chapter2/Figs/Vector/}{Chapter2/Figs/}}
\fi

\section{Introduction to micro-fabrication of reactor devices}\label{sec:2.1}

The fabrication of microfluidic devices in polymers or soft metals has been
performed for several decades \cite{Nguyen,Kralj2007, Kumar2011, Narayanan1970,
R1969, Walia1976, VanLandeghem1980, Asai1985,Ito1987, robbins_model_1988,
Kok1996, Luo2000}. The reason polymers are the main source material for the
development of these research chips is their beneficial properties in comparison
to glass or silicon chips. One of these reasons is the relative low cost as
prototyping material \cite{Nguyen, mansur2008, Nguyen2002}. Polymers have the
advantage that they exhibit a large range of mechanical strength, chemical
stability, optical properties. Because of this, they can be tailored for each
specific application. Glass has comparable properties but is much harder to
process, often involving etching steps similar to processing of silicon chips.
The polymer chips do not only have positive properties. For example, they are
only applicable for limited cases when it comes to temperature ranges and
solvent compatibility is also a problem. Although polymers can often be
processed much easier than glass or silicon, they have less surface finishing
possibilities, leaving them with a rougher finish than the glass chips. In
return the processing of the polymer chips is only a fraction of the time and
can be done in one single step. This is in contrast to the etching of glass or
silicon chips. The use of these polymer chips has opened a multitude of doors
for new or revamped processes currently not controllable or unachievable
\cite{Lewandowski2015}. The processing of polymer chips can be done in several
ways and will be described in the following paragraphs.

%book Microfabrication for microfluidics \cite{Lewandowski2015}

\begin{table}
\centering
\caption{Selection of manufacturing techniques for micro-machining of fluidic chips.}
\label{tab: selection of manufacturing techniques}
\begin{tabular}{cc}
\hline
Additive manufacturing & Subtractive manufacturing \\ \hline
Primary casting & Lithography \\
Replica molding & US-displacement/welding \\
Micro-injection molding & Laser ablation \\
Thermoplastic reshaping & Micro milling (CNC) \\
3D printing & \\ \hline
\end{tabular}
\end{table}

\Ref{tab: selection of manufacturing techniques} lists several subtractive and
additive manufacturing techniques currently used for microfluidic and mesoflow
chip fabrication.

\textbf{Lithography} is one of the most common techniques, also found for the
production of electronic chips \cite{Prakash2014}. The process in general
consists of applying a thin layer of resist (typically SU-8) on the substrate,
illuminating the substrate with either ultraviolet (UV) or laser light through a
mask. This alters the exposed parts of the resist, which can be removed
afterwards. After this, the substrate is etched, for example by reactive ion
etching (RIE), or used as a basis for additive manufacturing. LIGA (Lithographic,
Galvanoformung, Abformung - german acronym) is another technique that allows for
high aspect ratio etching/fabrication (height/width and slope of the wall). It
is comprised of the following steps:

\begin{itemize}
 \item expose a thick layer of resist to UV- or X-ray
 \item remove the exposed resist
 \item apply a complementary metal structure
\end{itemize}

This allows the mold to be used as negative for embossing or as mold insert for
further processing with high aspect ratios of metallic structures
\cite{Ehrfeld2001}. A commercial company using this technique is Ehrfeld
Microtechnik, which produces microfluidic structures and mesoflow reactors in
this manner.

Both laser ablation and computer numerical control (CNC) (micro-)milling are
evaluated and shown in the following sections. CNC milling is then used for the
remaining chapters (\ref{ch:chapter3}, \ref{ch:chapter4} and \ref{ch:chapter5}).
The remaining techniques found in \ref{tab: selection of manufacturing
techniques} were not used in this thesis and are not discussed further. They
however have can be found in \citep{Lee2010}.

\section{Laser ablation - evaluation}

Laser ablation of a microfluidic chip is another manufacturing process allowing
for precisely engineered features. This technique has the benefit that it does
not require a mask, resulting in faster computer aided design (CAD) to
fabrication. The laser source is commonly either a carbon dioxide (\ce{CO2}) or
an excimer laser. The \ce{CO2} laser operates within the infrared spectrum with
high power levels. In contrast, the excimer lasers operate in the UV range (193
- 353 nm). These laser sources are more costly than a \ce{CO2} laser, they have
the advantage that the material is not overly heated (heat affected zone is
minimal). The main drawback of this technique is the problem of
profiling/tapering as a result of focal point concentration of lasers (and
lights sources in general).

\subsubsection{screening results}

To evaluate the effect of this tapering, a slice was taken from a fluor-carbon
rubber chip treated with a \ce{CO2} laser that was processed by laser ablation
(\ref{fig:laser ablatie tapering}). The tapering of the laser light is apparent
and illustrates the encountered problems such as wall tapering and depth control
. The cross section is that of a bifurcating flow distributor. The center cut is
a main feed line from which channel branch (bifurcate) reducing the channel
cross section by $1/3^{rd}$. This division should be symmetrical on both sides
of the main channel. From our experience it shows that laser ablation is a
viable processing method when doing through-cuts effectively circumventing the
problem of irradiation time and intensity.

\begin{figure}[H]
 \centering
 \includegraphics[width=0.65\textwidth]{laser_ablatie_tapering.png}
 \caption{Cross section of a bifurcating flow distributor generated with
 laser ablation of a fluor-carbon rubber sheet of 1 mm thickness. Non-uniformity
 and severely inclined channel walls make this technique unsuitable to create
 channels with well controlled flow resistances.}
 \label{fig:laser ablatie tapering}
\end{figure}

This technique was further evaluated with optical imaging for surface roughness
(\ref{fig:Laser ablatie wyco}) highlighting a significant issue arising from
debris deposition on the chip surface processed with laser ablation. By exposing
a location with the laser light, the molecules vaporize but cannot be removed in
time and solidify on the surface of the chip, resulting in spikes on the
surface. This makes it very hard to seal the reactor chips by means of thermo-
bonding or other techniques used to seal substrates.

\begin{figure}[H]
 \centering
 \includegraphics[width=0.65\textwidth]{Wyco-Laserablatie.png}
 \caption{Optical profiling image of a 30 $\mu m$ hole made in borosilicate
 glass clearly showing the leftover debris from the ablation collecting on
 the surface. This makes it nearly impossible to thermo bond several layers.
 (Peak height goes up to $30 \mu m$ for a $50 \mu m$ hole)}
 \label{fig:Laser ablatie wyco}
\end{figure}

After these key issues, the thesis diverted its attention towards CNC milling
which was available at a low cost. This increased the prototyping time
and has a higher depth accuracy when partial cuts are required.

\section{CNC micro-milling - evaluation}\label{sec:micro-milling}

All the reactors, evaluated throughput the remainder of the thesis, have been
developed and produced using micro-milling. This is done using the
(computational numerical control) CNC milling machine m7 from Datron available
at our department. This method allows for rapid prototyping of several reactor
designs. The downside is that the equipment can only work around 3 axes (x, y,
z) making it impossible to do some complex designs in a single milling
operation. The machine allowed for model verifications and was a key tool in several
publications ranging from extractors, mixers and flow distributors from both
polymers and soft metal source materials
\cite{sultan_experimental_2012, Goovaerts2014}.

The machine can handle surface milling tools from 0.1 mm until 6-8 mm and drills
from 0.150 mm diameter up until 6 mm. The 0.6 kW powered spindle rotates between
18 000 and 50 000 revolutions per minute. The key for optimal surface finish is
the correct chip load \footnote{the chip thickness per tooth is called the chip
load} (\ref{fig:chip load}). By optimizing the chip load the tool lifespan can
be maximized whilst this assures that the tool deflection is minimized. This in
turn results in a cleaner, more uniform cut and finish of the device.

Tools break under high chip load because the tool cannot remove the bits fast
enough or takes a chunk from the material that is to large. If the chip load is
to low, the tool starts to rub the material, resulting in overheating and
melting. Since the devices are built from polymers, mostly thermoplastic
polymers, the heating of the device will make the chip unusable. The same is
true when the chip load is too high and they cannot be cleared anymore.

To achieve a good chip load (CL) one can calculate the optimal feed rate
(FR)\footnote{x,y movement in m/min} with accompanying rotation speed (RPM):

\begin{equation}\label{eq:feed rate}
FR = RPM x T x CL
\end{equation}

with T the amount of tooth on the tool. The ideal chip load is in the range of
0.001 to 0.002 inch per tooth. For most tools over 1 mm there are some
guidelines that follow the generic formula. This starts to pose a problem when
the tool diameter is reduced below 1 mm. When using the regular guidelines, the
tools start to deflect (\ref{fig:tool deflection}), resulting in broken tools
and bad cutting results. A second problem with such small tools is the fact
that each tool and piece has its own-frequency at which it vibrates. If the
tool vibration is in the range of the tool diameter, one can imagine that the
vibration results in a very low accuracy when the vibration is in the same
order of magnitude as the tool width. Besides low accuracy due to vibration the
tool was exposed to other than intended forces increasing the risk of tool
breakage.

\begin{figure}[H]
 \centering
 \subfloat[]{
 \includegraphics[width=0.1\textwidth]{tool_deflection.png}
 \label{fig:tool deflection}
 }
 \subfloat[]{
 \includegraphics[width=0.55\textwidth]{chip_load.png}
 \label{fig:chip load}
 }
 \caption{\textbf{(a)} Deflection of a regular milling tool under extensive
 side load and \textbf{(b)} an illustration of chip load.}
\end{figure}%


The supplied tools have a diameter offset of 0.03 $\mu m$ or 1 \% (fabrication
tolerance of supplier). The accuracy of the stepper motors in x and y direction
was 0.5 $\mu m$. To assure the correct xy-position and z-depth, the device is
supplied with a sensitive xyz-sensor. This sensor makes it possible to scope
the surface for waves, changes in z-height can be compensated for. This sensor
also allows for a re-calibration on chips that had to be processed in several
steps often including repositioning. The accuracy of the sensor was 1 $\mu m$
in z direction while accurate to 5 $\mu m$ in xy direction when the tool was
calibrated according to the predefined procedure.

As a rule of thumb, a tool with the minimal length required for the full depth
cut should be used rather than a longer tool. This seems evident and is
confirmed when thinking about the deflection increase with total length. To
further reduce the chance of deflection, the tool stickout\footnote{part of tool
cutting range cutting air} should be minimized. This reduces tool deflection by
equilibrating the applied force on the tip and reduces vibration by giving more
supporting structure. By reducing the number of flutes, the overall rigidity of
the tool and chip clearance is increased.

By pre-drilling a hole in a pocket or approaching the plate from the outside
rim, it was possible to supply the remaining milling steps to have a significant
volume for chip removal. This was further increased by approaching pocket and
rough milling by helical shavings of the substrate, for the full depth of the
tool or the required total depth.

\subsection{Micro-milling of polymers - optimized settings}
\label{subsec:milling settings}

Since the reactor designs for this thesis required machining with tools less
than 1 mm width, some iterative steps have been undertaken to optimize the tool
lifespan and finishing quality of the chips. This procedure gave a good
starting point for further processing of the reactor chips. These experiments
resulted in a chip load of 0.00135 mm x tool diameter (mm) given in
\ref{tab:tool settings}. The RPM settings were maximized to expedite the
milling process but were limited by the maximal xy-movement of the stepper
motors, hence the reduction in rpm for larger tools. These settings, combined
with previously mentioned considerations made it possible to mill a chip that
required minimal post-processing/cleaning.

\begin{table}[]
\centering
\caption{Chip load and RPM settings used for micro-milling of PMMA using
the Datron m7 micro-milling machine.}
\label{tab:tool settings}
\begin{tabular}{ccc}
\hline
tool diameter (mm) & chip feed thickness ($\mu m$/tooth) & RPM (1000x) \\ \hline
0.1 & 1.35 & 45 \\
0.2 & 2.70 & 45 \\
0.3 & 4.05 & 45 \\
0.4 & 5.40 & 45 \\
0.5 & 6.75 & 45 \\
1.0 & 13.50 & 45 \\
2.0 & 27.00 & 30 \\
3.0 & 40.50 & 30 \\
6.0 & 81.00 & 30 \\ \hline
\end{tabular}
\end{table}

\subsubsection{Micro-milling of polymers - results}

\begin{figure}[H]
 \centering
 \subfloat[]{
 \includegraphics[width=0.45\textwidth]{bad_milling.png}
 \label{fig:bad milling}
 }
 \subfloat[]{
 \includegraphics[width=0.45\textwidth]{manual_cleaning.png}
 \label{fig:after cleaning}
 }\\
 \subfloat[]{
 \includegraphics[width=0.45\textwidth]{good_milling.png}
 \label{fig:clean milling}
 }
 \caption{Microfluidic chip produced with micro-milling. \textbf{(a)}
 A chip fabricated with rough milling without post processing. \textbf{(b)}
 A chip that has been manually cleaned for debris. \textbf{(c)}
 A chip fabricated with optimal settings without any post-processing.}
 \label{fig:milling quality}
\end{figure}%

\Ref{fig:milling quality} shows several stages of the milling process
evaluation of PMMA. \Ref{fig:bad milling} shows a chip processed with a 100
$\mu m$ tools at 45 000 RPM with a low chip load, resulting in rubbing of the
surface. This rubbing resulted in a large fraction of the fringes visible at
the upper edge. These fringes could be removed using several post-processing
techniques, resulting in a cleaner chip (\ref{fig:after cleaning}). The post-
processing can be either mechanical removal or diluted solvent treatment with a
90/10 \% water-acetone mixture. Each added step during the development of a
chip induces chances of failure. \Ref{fig:clean milling} shows the same chip
using the optimized settings from \ref{tab:tool settings} without any post-
processing (cleaning of debris or repairs).

The used substrates were limited to polymers and brass or aluminum to maximize
tool lifespan. For most of the experiments, the additional property of
reactor/chip transparency was required. This directed the choice of polymer
towards polycarbonate (PC), poly-methyl-methacrylate (PMMA) or cyclic olefin
copolymer (COC). These polymers are transparent for the correct wavelength
used in fluorescence microscopy allowing for a direct on-chip measurement of
mixing. The polymers also show little swelling when using reactions in aqueous
environment \cite{Ehrfeld2001, Lee2010}. The sheets were delivered from cast PMMA from
plates of 1 until 15 mm thickness and had a z-height deviation of 200 $\mu m$ so
the surface was face milled prior to milling the channels or features in the
chip. This assured an equal depth over the entire reactor.

After processing, the remaining debris needs to be removed or cleaned using
pressurized air. By using a 90\\10 \% water-acetone mixture it was also
possible to etch the smallest fractions of debris without damaging the reactor.

\subsection{Sealing the reactor}\label{subsec:sealing the reactor chip}

Since the CNC milling machine is a subtractive method, each reactor had to be
sealed. In most articles, the reactor was built in poly-dimethyl-siloxane
(PDMS), which is much less rigid allowing it to be sealed with a glass plate
clamped on top \cite{Tan2006, Shui2007, Zhao2011b, Nguyen2002, Garstecki2006,
Assmann2011a, nguyen_optical_2006}. Doing so, the chip cannot withstand fluid
pressures, typically only less than 1 bar, often resulting in leakage. Besides
the risk of leakage, by clamping glass on the PDMS chip, the channels start to
deform inducing other than intended flow patterns or even channel blockage.

Since PMMA and PC are transparent for the UV light and the material is
relatively cheap, it was possible to perform an irreversible sealing step by
effectively thermo welding a top plate on the substrate. To do this, both the
reactor and the top plate were clamped between 1 mm aluminum plates with paper
clamps. This group was then put inside a hot air oven at 150
$^{\circ}\mathrm{C}$ for 20 min. This assured that the entire chip was able to
surpass the glass temperature allowing the polymers to intertwine creating an
irreversible bound.

This thermo welding can be extended to a more precise procedure applying laser
light on designated locations. This effectively welds the layers together on
the interface creating a guaranteed seal with minimal thermal impact on the
rest of the features \cite{Nguyen, Nguyen2002}.

An alternative to thermo welding was the use of solvent assisted bonding. Using
this technique a fraction of the solid surface is dissolved, making it possible
for the surfaces to intertwine during drying \citet{Umbrecht2009}. There is a
slight preference for PMMA towards di-chloromethane (DCM) as solvent. The PMMA
surfaces were aligned and a droplet of DCM was applied on the edge. By means of
capillary force, the DCM would flow in between the two surfaces. After
evaporation, the surfaces are bonded. Since this is quasi instantaneous it was
a favorable method for easy features but it led to a huge risk of deformation
or linkage when the DCM came into contact with one of these inner channels.
This resulted in blocked channels or broken features.

% TODO-DONE: include procedure for DCM solvent-bonding

%http://www.cfd-online.com/Wiki/Navier-Stokes_equations

%********************************** % Third Section ***************************
%removed FITC measurement and shifted it to chapter 4.


%********************************** % Fourth Section ***************************
%\section{Computational fluid dynamics} \label{sec:2.4}
\section{From idea to realization}

To expedite the process of reactor design, 3D models were generated using
Solidworks. This has some key advantages compared to conventional 2D models. A
2D model is easy to design but is hard to imagine when using complex
geometries. When making the switch to 3D models, they can be used for both the
visualization, production and modeling (being it strength or computational
fluid dynamics - CFD).

Working with 3D models in Solidworks opened doors towards CFD and
visualization/fitting. Solidworks is a computer aided design (CAD) and
computer-aided engineering (CAE) software package by Dassault systems. It is a
solid modeler utilizing a parametric feature-based approach. This means that a
2D relational sketch is drawn, which is extruded or cut from another feature.
This allows for fast visualization of an idea and optimization. Due to the
relational sketches, it is possible to make a design that is modular, enabling
a fast optimization of the design. This approach also allows to test fit all
remaining components such as external connections, heating plates etc prior to
fabrication.

\begin{figure}[H]
 \centering
 \subfloat[]{
 \includegraphics[width=0.45\textwidth]{3D_design_example.png}
 \label{fig:3d example}
 }
 \subfloat[]{
 \includegraphics[width=0.45\textwidth]{3d_velocity_field.png}
 \label{fig:3d velocity field}
 }
 \caption{Example of a 3D micromixer model of \textbf{(a)} a low pressure mixer
 and \textbf{(b)} the calculated slice of the velocity profile [m/s] in this
 mixer.}\label{fig:3D model}
\end{figure}

Once the design is modeled it is possible to either start the operations for
the milling process (\ref{sec:micro-milling})
using solidcam and/or perform a CFD analysis. The analysis would give a first
insight in several key points of the design searching for death zones,
locations with high energy costs (pressure loss) or features that do not
attribute to the reactor (\ref{fig:3D model}). This CFD analysis can be done
either in solidworks itself or linked to an external package such as Comsol or
Ansys CFD. The latter two packages are commercially available and are
designated packages for CAE and more specifically for physics calculations. For
this thesis, both packages have been used to solve fluid dynamics and heat
transfer partial differential equations. Both work in the same principle of
finite elements method
\cite{Griffini2007, hjertager_cfd_2002-1, Ball1997, gavi_turbulent_2010-1,
Hessel2005, Wong2004}. This means that the model is divided in discrete
(un)ordered cells defined by nodes and edges (\ref{fig:grid}).

\begin{figure}[H]
 \centering
 \subfloat[]{
 \includegraphics[width=0.45\textwidth]{grid_mesh.png}
 \label{fig:grid}
 }
 \subfloat[]{
 \includegraphics[width=0.45\textwidth]{plug_injection.png}
 \label{fig:plug injection}
 }
 \caption{Example of a bore column (diameter: 5 cm) header with a frit for a
 study of jetting due to inlet problems. \textbf{(a)} an ordered mesh and
 \textbf{(b)} the calculated plug after the injection using this grid.}
 \label{fig:grid and plug injection}
\end{figure}

Depending on the package, the iterative calculation of the Navier-Stokes
equation (\ref{eq:navier stokes}) is done by differentiation over the center of
a cells (defined by nodes and edges) or the edges of a cells. This can then be
applied to calculate a plug injection to analyze peak broadening and injection
problems using regular fritz in HPLC columns (\ref{fig:plug injection}).

\begin{equation}\label{eq:navier stokes}
 \frac{\delta u}{\delta t} + (u. \Delta)u = \nu \Delta^2u
\end{equation}

Once the design was verified and found suitable for experimental validation,
the design was migrated towards solidcam, producing machine code that can be
used in the milling machine. This software package allows for fast analysis of
the undertaken steps and tools displaying all interactions with the plate/chip
in the machine and the estimated production time.

\section{Conclusion}

For \ref{ch:chapter3}, \ref{ch:chapter4} and \ref{ch:chapter5} customized
reactors had to be fabricated for which this chapter listed several techniques
and evaluated two concretely for further use. The reactors were all fabricated
using CNC-milling since laser ablation did not result on qualitative reactors.
The reactor in
\ref{ch:chapter6} did not require a customized reactor and is build from
stainless steel tubing and connections.

The first evaluated technique was laser ablation. This technique performs well
for through-cuts and especially features where a sloped wall is within the
tolerance level (angle above 80 degrees). This slope in itself is property of
the used technique and thus far can only be reduced by refocusing on several
Z-heights when performing a through-cut. When trying this with partial cuts in
the surface, the tapering was more pronounced. Furthermore, when using this
technique for the creation of channels in the surface, the focal point and
tapering makes it hard to reproduce the reactor several times and is subject to
conditions such as exposure time, effective depth, number of passes ea. If the
vaporized material is not removed properly, the debris will concentrate on the
edges or on the surface of the feature, resulting in bumps and impurities shown
in the optical profiling image as uneven surface. In the end this technique
comes in as a very powerful for micro-precise contours and cuts as long as one
respects the tapering of the walls and perform through-cuts rather than partial
cuts with proper debris removal.

The second evaluated technique, micro-CNC-milling, can result in partial cuts
with a clear finish. Nonetheless it is limited in its applicability. With
regular CNC milling and tools above 2-3 mm, several easy guidelines and formulas
can be utilized. These guidelines are no longer valid when reducing tool sizes
below 2-3 mm. The main point to be taken into account is the tool deflection and
chip load per tooth with such small tools. For this thesis PMMA sheets were
processed using the CNC milling technique. The optimal settings were
evaluated and found to be a chip load of 0.0135 mm per tooth per 1 mm of tool
diameter as adequate. Less than this resulted in tool rubbing on the surface
rather than cutting material. This rubbing forces the PMMA to surpass the glass
temperature, melting or burning the material. This in itself broke more tools
than required and created rougher or clogged channels.

In general, every technique has benefits of which the above mentioned two
techniques are some of the less expensive ones. The most accurate technique is
that of LIGA, which is far more expensive but allows for materials that can be
sintered with lithographic accuracy.

The use of CNC milling was utilized in \ref{ch:chapter3}, \ref{ch:chapter4} and
\ref{ch:chapter5} allowing for fast screening of geometrical designs in the
reactors using the optimized settings concluded in this chapter. These reactor
designs were then studied upon for both purification/concentration.

%********************************** % Reference Section ***********************
\setcounter{NAT@ctr}{0}
\bibliographystyle{unsrtnatV2}
\bibliography{References/libraryV2}
