\chapter{Mesoflow reactors: Multi-component solvent extraction}\label{ch:chapter5}
\vfill
\begin{flushright}
This chapter is submitted to Separation and Purification Technology \\
\textbf{A robust multistage mesoflow reactor for liquid-liquid extraction,
separating Co/Ni with Cyanex 272}\\
Vandermeersch T., L. Gevers, W. De Malsche
\end{flushright}
\clearpage
%\begin{flushright}

Purification of a mixture of metals in solution is the second step in the
refinement of metals occurring the hydrometallurgic industry. This is referred
to as solvent extraction wherein two immiscible liquid phases are contacted
between which the metals will partition themselves over both phases. Solvent
extraction is a key principle for separations of components that requires
relatively large separation times due to large droplet size distributions,
which often is the limiting capacitive factor illustrated in \ref{ch:chapter4}.
A continuous mesoflow reactor setup was evaluated for solvent extraction and
phase separation in terms of efficiency on Co/Ni separation with Cyanex 272 as
extractant in a kerosene mixture. Several mixer types from \ref{ch:chapter4}
have been evaluated for the solvent extraction combined with an inhouse
developed thin film coalescer/separator expediting separation times, resulting
in a settling volume that was only 1 to 2 times the volume of the mixer
effectively reducing residence time. This was mainly attributed to the droplet
homogeneity generated in the mixer, resulting is a distinct separation time.
The thin film coalescer partitions in the settler expedited the phase
separation further reducing the settler volume. Overall this chapter has shown
that continuous mesoflow reactor setups can be utilized for small-scale liquid
extractions with narrowly defined droplet sizes enhancing the phase separation
time making it a viable screening utility.

%\end{flushright}
\pagebreak

%*******************************************************************************
%****************************** Fifth Chapter *********************************
%*******************************************************************************

% **************************** Define Graphics Path **************************
\ifpdf
 \graphicspath{{Chapter5/Figs/Raster/}{Chapter5/Figs/PDF/}{Chapter5/Figs/}}
\else
 \graphicspath{{Chapter5/Figs/Vector/}{Chapter5/Figs/}}
\fi

\section{Introduction}\label{sec:chapter5-introduction}

Purification of a mixture of metals in solution is the second step in the
refinement of metals occurring the hydrometallurgic industry. This is referred
to as solvent extraction wherein two immiscible liquid phases are contacted
between which the metals will partition themselves over both phases. In
\ref{ch:chapter4} a discussion and experimental evaluation was performed,
regarding the energy dissipation in microflow reactors and the resulting droplet
size and mass transfer rate influence. Several of these mixers have
been selected for a case study of the separation of Co/Ni ($Cl^{1-}$ in aqueous
phase) with Cyanex-272 in kerosene (organic phase). The results are compared with
batch processes and internally with several other mixer types.

Solvent extraction is an alternative to component separation where distillation
or crystallization are impractical both in medical and industrial applications.
Conventional solvent extraction is performed in continuous stirred tanks in
series (CSTR) , batch vessels, centrifugal extractors or membrane extractors
\cite{Kolbl2011, Baier1999, Kislik, Tamhane2014, Kumar1983}. All but the latter
type are relying on the dispersion of one liquid into the other liquid to
facilitate mass transfer over the interface.

When the extraction is performed under the form of a reactive extraction the
extraction rate is generally accepted to be limited by mass transfer rather
than reaction rate in case of reactive extraction. The Damk\"ohler (II) number

\begin{equation}\label{eq: Damk}
Da_{II} = \frac{kC_0^{n-1}}{k_g a}
\end{equation}

describes the ratio between the reaction rate and the mass transfer rate with k
the reaction rate, C the concentration, n the reaction order, $k_g$ the global
mass transfer rate and a the interfacial area. An example of the evolution of
the Da(II) number as function of the droplet diameter is given in
\ref{fig:damkohler} for a first order reaction. \cite{DeVoorde2006,
Koncsag2011}

\begin{figure}[H]
\centering
	\includegraphics[width=0.75\textwidth]{Damkohler.png}
	\caption{Damk\"ohler number as a function of the droplet diameter
	for a $k=10^{-3}~[s]$, $k_g=10{-9}~[mol/(m^2.s)]$ and $n=1$}
	\label{fig:damkohler}
\end{figure}

In the case of first order reactions ($k=10^{-3}~[s]$) with a mass transfer rate
of $10^{-9} [mol(m^2.s)^{-1}]$, the Da(II) number is higher than 5 when droplets
are 30 $\mu m$. This means that the reaction rate is 5 times higher than the
mass transfer which is the limiting step.

To expedite the extraction rate, the interface between the two phases should be
maximized. The only way this can be done in the case of dispersions is by
decreasing the dispersed phase droplet size. To achieve smaller droplets, the
liquids have to be mixed vigorously to generate this small droplet size. By
doing so, due to difference in shear rate (close and far from the mixer axle)
the droplet size distribution is large, resulting in micron sized droplets
whilst others are still in the mm-scale, resulting in less optimal conditions
for extraction \cite{Narasimha2005}. A significant problem arising with
decreasing droplet size is the increase in phase separation time. The phase
separation is in most cases defined by the difference in density in these
reactor types. To achieve full separation the smallest droplets have to phase-
separated as well. The rate at which the droplets migrate due to density
differences is expressed as the terminal phase separation velocity:

\begin{equation}\label{eq:phase separation speed}
v_t = \sqrt{4g d (\Delta \rho) (3 \rho_{cont}C_{drag})^{-1}}
\end{equation}

In case of low Re numbers (less than 2) the drag coefficient ($C_{drag}$)
scales linear with the Re number resulting in a terminal phase separation velocity:

\begin{equation}\label{eq:phase separation speed laminar}
v_t = \frac{1000 g d^2(\rho_h - \rho_l)}{18 \mu_l}
\end{equation}

While the surface-to-volume ratio is proportional to $a = 6.d^{-1}$ \cite{Geankoplis,
Perry1997}. In batch reactors a large droplets size distribution is
common, the terminal velocity of the smallest droplets is the rate determining
step of phase separation, resulting in large settler/mixer volumes
\cite{FathiRoudsari2012, Mukherjee2012}. The cyclone extractor compensates the
need for large settler volumes by maximizing the angular velocity, but the
velocity is still proportional to $d^{2}$ (\ref{eq:phase separation speed
laminar}) \cite{Liu2012d}. It is more efficient method of phase separation for
large droplets but the smallest fraction still typically has to be separated in
a secondary separator (cutoff size).

To omit the need for phase separation a lot of research has been conducted
towards membrane extractors in the form of flat sheet membrane extraction and
hollow fiber extraction \cite{Swain2015, Hereijgers2015c, Hereijgers2015}. By
utilizing these reactor types, the membrane acts as a phase barrier allowing for
liquid-liquid contact without dispersion, thus making phase separation
obsolete. The main drawback lies within the fact that the membrane performance
is restricted by the Laplace pressure and wettability of both liquids. This has
been investigated in flat membrane extractors and in hollow fiber
membranes \cite{Hereijgers2015c, Swain2015, Geankoplis}.

Coming back to dispersive extraction, the most efficient way to perform the
extraction and separate the phases is by increasing the density difference or by
confining the droplet size distribution to a narrow range. Such narrow control
on droplet size distribution cannot be achieved in classical setups.
Intensification of a chemical process aiming at more effective chemistry and
energy transfer go hand in hand with the miniaturization of chemical reactors.
These mesoflow reactors are carefully defined reactors containing a number of
specific features such as pillars, obstructions and orifices, using the high
surface-to-volume ratio and the short length scales of the mesoflow reactors.
These reactors could be used for rapid experimentation leading to shortened
product development cycles \cite{VanGerven2009}.

These reactor types rely on geometrical influences for dispersion often
resulting in a narrower droplet size distribution \cite{Jovanovic2011,
Vacassy2000}. At low flow rates the continuous flow reactors (CFR) can be
operated in a segmented flow regime, resulting in instant phase separation but
with limited throughput
\cite{Jovanovic2011, Vacassy2000, Tan2012, Knauer2011, Harries2003,
Tsaoulidis2015, Khan2004, Zhao2011b, Fu2012, Hoang2014}. When increasing the
flow rate the transition from segmented to dispersive flow has been studied
extensively \cite{Kenig2013, Kashid2009,Woitalka2014}.

Several CFR mixer types have been evaluated in combination with an inhouse
developed settler that utilizes wettability of a solid interface in the settler
as coalescer, which results in a significant reduction in total residence time
in single and a multistage setup. This setup has been evaluated in single and
multistage for Co/Ni ($Cl^-$) separation with saponified Cyanex 272 in kerosene.

The results obtained in this chapter demonstrate that exploiting the beneficial
properties of process intensification by size reduction yields a faster solvent
extraction rate whilst increasing the phase separation rate by a more precise
droplet size distribution control compared to conventional setups.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Experimental methods}\label{sec:experimental-methods}
\subsection{Reagents}

The solvent (escaid 110 - Shell) is a kerosene mixture of C12-C14 alkanes. The
extractant (Cyanex 272) was obtained from Cytec Inc. and was used without
further purification. The organic phase consisted of 50 wt\% Cyanex 272 in
escaid 110. The extractant was saponified for 60\% adding a stoichiometric
amount of 10 M NaOH solution to the organic mixture whilst cooling it under tap
water until a homogeneous phase was obtained. The aqueous phase (industrial feed)
contained 35 g\/l Co and 9 g\/l Ni in chloride medium which was replenished on daily
basis to prevent precipitation.

\subsection{Equipment}

The mesoflow reactors used for the experiments were developed and fabricated
according to the methods in \ref{ch: material and methods}, evaluated in
\ref{ch:chapter4} and are illustrated in \ref{fig:designs}. The separation of
the organic and water phase was facilitated in an in-house developed settler
with a serpentine design enforcing droplet coalescence on the surface and due to
angular acceleration in bends depicted in \ref{fig:settler-design}. The settler
has a thin film contacting zone (0.3 mm depth) alternated by a settling zone (3
mm width). The entire volume of the settler is 25 ml. The settler was built from
1 cm thick PMMA plates, which were processed in a similar fashion as for the
vortex reactor. The average residence time where extraction might be considered
viable is the first entry slot.

\begin{figure}[H]
\centering
 \includegraphics[width = 0.95\textwidth]{settler.png}
\caption{Settler layout, displayed settler of 25 ml. At the top showing the
side view and bottom showing the top view of the settler. Total height is 5 cm,
 wide partitions define separation zones of 1 mm width and narrow zones depict
 the coalescence zones of 0.3 mm width.}
\label{fig:settler-design}
\end{figure}

\subsection{General procedure}

The aqueous and organic phase were supplied to the mixer/settler reactor setup
using two pairs of syringe pumps (Legato P200, kd scientific, USA). These pumps
work in pairs with three-way valves in between allowing for continuous operation
with little pressure fluctuations. Each stage and phase has its own syringe pair
working in alternating direction. Whilst one syringe is supplying fluid to the
reactor, the other syringe withdraws the same amount of liquid from the settler.
By doing this in pairs, the levels in the settler remained constant and the
settler could be operated under pressure allowing for higher temperatures by
reducing safety risks due to vapor formation of the organic phase. The scheme
for the three stage setup is shown in \ref{fig:reactor setup} with dashed lines
as water phase and full lines as organic phase.

\begin{figure}[H]
\centering
 \includegraphics[width = 0.95\textwidth]{staging_setup.png}
\caption{Triple stage illustration of the reactor setup as a counter-current
 liquid-liquid extraction setup. The dashed line represents the water phase
 and the full line represents the organic phase. The pumps are build from
 two syringe pumps working in pair for each phase and a single stage setup is
 highlighted in the middle.}
\label{fig:reactor setup}
\end{figure}

Each measurement was equilibrated for a minimum of 3 times the total residence
time. If the setup volume was 100 ml and the flow rate was 20 ml/min the setup
would be operated for 15 min prior to sampling for analysis. Before running
experiments, the settler was primed and leveled manually.

\subsection*{Analysis methods}

The treated water phase was analyzed for Co and Ni concentration using inductive
coupled plasma - atomic excitation spectroscopy (ICP-AES). From these values the
extraction efficiency is defined:

%From these results the extracted fraction of Co or Ni could be determined with
%\ref{eq:extraction fraction} with $M^{2+}$ either Co or Ni.

\begin{equation}\label{eq:extraction fraction}
 E = \frac{D_{M_{aq}^{2+}} (vol_{o}/vol_{a})}{D_{M_{aq}^{2+}}(vol_{o}/vol_{a}) + 1 }
\end{equation}

The distribution coefficient ($D_{M^{2+}}$) describes the distribution of the
component over the organic and water phase:

\begin{equation}\label{eq:distribution coefficient}
 D_{M^{2+}} = \frac{C_{M_{org}^{2+},end}}{C_{M_{aq}^{2+},end} }
\end{equation}

The ratio of the distribution coefficients describes the selectivity of the
extraction:

\begin{equation}\label{eq:sel}
sel = \frac{D_{C10^{2+}}}{D_{Ni^{2+}}}
\end{equation}

The organic phase was analyzed for the entrainment of water using a centrifuge
spinning at 3000 rpm for 15 min, after which the weight fraction of the decanted
water was measured.

Batch experiments were performed to determine the equilibria using a double
walled glass batch reactor of 1 l with PTFE baffles and agitator. The reactor
was heated to 55 $^{\circ}\mathrm{C}$ using water from the warm water bath
flowing through the double wall.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Results and discussion}\label{sec:results}

Metal solvent extraction is typically an equilibrium reaction during which
protons are exchanged for metal ions, for the present case: Co and Ni. The ratio
at which these components are divided over the organic phase and the aqueous
phase (D) is, amongst other parameters, regulated with pH. Therefore, it is
critical that the pH is controlled and monitored in situ or alternatively the
protons can be exchanged prior to extraction (saponification of the organic
phase). The saponification proceeds according to:\\

\begin{equation}\label{eq:reaction cyanex saponification}
 Na_{aq}^{+} + 1/2(HA)_{2,org} \rightleftharpoons NaA_{org} + H_{aq}^{+}
\end{equation}

which in turn reacts with the metal ion (Co or Ni) \cite{Sarangi1999}:\\
\begin{equation}\label{eq:reaction cyanex metal}
 M_{aq}^{2+} + 2(HA)_{2,org} \rightleftharpoons MA_{2}.(HA)_2 + 2 H_{aq}^{+}
\end{equation}

with A the deprotonated cyanex and $M^{2+}$ either $Ni^{2+}$ or $Co^{2+}$.
Furthermore, the overall equilibrium constant K is:\\
\begin{equation}\label{eq:equilibrium constant}
 K = \frac{[MA_2.(HA)_2]_{org}[H^{+}]_{aq}^{2}}{[(HA)_2]_{org}^{2}[M^{2+}]_{aq}}
\end{equation}

\subsection{Single stage experiments: mixer comparison}

In \ref{ch:chapter4} it has been shown in earlier work that there is a distinct
correlation between energy dissipation ($\varepsilon$ [W/kg]) and the
interfacial surface area (a) in two phase flow under laminar conditions for the
vortex and LTF-MX reactors
\cite{Vandermeersch2015}. In that publication it was demonstrated that the LTF-
MX has a higher $k_la$ value (creating the highest surface area, smallest
droplets) than the vortex mixer for a given amount of energy
\cite{Vandermeersch2015}. The residence time is generated using a 1/8 inch PFA
spiral after the mixer since the mixers have a fixed volume. For the evaluation
of the mixer performance on the extraction, all parameters, except the mixer
design itself, were kept constant (Q, T, O/A ratio and initial concentration).
This allows for a direct comparison, although it has to be mentioned that
variations due to droplet coalescence in the spiral may occur as well. This
decay and coalescence of droplets in the spiral is assumed to be similar for all
mixers.


\begin{figure}[H]%effect of temperature, XRD and LS
\centering
 \subfloat[]{
 \includegraphics[width = 0.45\textwidth]{mixer_effect_sel.png}
 \label{fig:mixer-effect-sel}
 }
 \subfloat[]{
 \includegraphics[width = 0.45\textwidth]{mixer_effect_ENi.png}
 \label{fig:mixer-effect-E.Ni}
 }
 \caption{\textbf{(a)} The difference between the LTF-MX and the vortex mixer
 on the selectivity (\ref{eq:sel}) and \textbf{(b)} the extraction efficiency
 (E, \ref{eq:extraction fraction}) of Co and Ni separation with CX272 using the
 vortex mixer. ($Q = 22 ml/min$, $[Co_{start}]= 37~g/l$ and $[Ni_{start}]=
 9~g/l$)}
 \label{fig:mixer influence}
\end{figure}

First, the extraction performance of the LTF-MX and the vortex mixer was
compared with that of a T-mixer. The results (\ref{fig:mixer influence})
illustrate that under similar conditions (energy dissipation rate, residence
time, O/A) the performance varies significantly. This is most pronounced when
looking at the selectivity of each mixer.

What is apparent from \ref{fig:mixer influence} is that the LTF-MX and the
vortex mixer perform best. This indicates that the energy input in the reactors
is used more efficiently than in the parallel or T-mixer. The latter two mixer
types rely on Rayleigh-Taylor effects, which are only mildly affected by the
geometrical impact. Since the Rayleigh-Taylor effect induces droplets due to
fluid-fluid shear, the interfacial surface is defined by this interaction rather
the LTF-MX result in a narrow droplet size distribution whilst those generated
by the Rayleigh-Taylor effect are known to have a broader range including
satellite droplets (extremely small droplets) \cite{Vandermeersch2015}. These
droplets are difficult to separate from the organic phase, resulting in a higher
aqueous entrainment.

To study the performance of the extraction in CFR more in detail both the LTF-MX
and the vortex mixer have been used.

%should we even talk about extraction ratio?

%
%\begin{figure}[h!, width = 0.95\textwidth]%effect of temperature, XRD and LS
%\centering
% \subfloat[]{
% \includegraphics[width = 0.45\textwidth]{Ratio_SS_effect_E.Ni.png}
% \label{fig:Ratio-D.Ni}
% }
% \subfloat[]{
% \includegraphics[width = 0.45\textwidth]{Ratio_SS_effect_D.Ni.png}
% \label{fig:Ratio-E.Ni}
% }
% \caption{Influence ratio organic over water phase}
% \label{fig:Ratio influence}
%\end{figure}

\subsection*{Equilibration time}

\begin{figure}[H]%effect of temperature, XRD and LS
\centering
 \subfloat[]{
 \includegraphics[width = 0.45\textwidth]{t_SS_effect_sel.png}
 }
 \subfloat[]{
 \includegraphics[width = 0.45\textwidth]{Multistage-t-efficiency-effect.png}
 }
 \caption{Influence of the residence time on \textbf{(a)} the extraction efficiency
 for both the LTF-MX and the vortex mixer (n=3) and \textbf{(b)} the selectivity
 in the vortex reactor in a single stage setup ($Q = 22 ml/min$, $[Co_{start}]= 37 g/l$ and $[Ni_{start}]= 9 g/l$ ).}
 \label{fig:residence time influence}
\end{figure}

\Ref{fig:residence time influence} depicts the influence of residence time on
the extraction efficiency for Co and Ni in the flow reactor for the LTF-MX and
the vortex mixer with an O/A ratio of 1.55 at 55 $^{\circ}\mathrm{C}$. Both
reactors only slightly differ in extraction rate and are at equilibrium after
approximately 1 min, in comparison the batch reactor reaches the same Co
extraction efficiency after 10 min. The only reason why the extraction reaches
equilibrium faster in the CFR is the intense mixing, resulting in a smaller mean
droplet size effectively reducing average diffusion distances and increasing the
mass transfer rate due to increased interfacial area. The interfacial surface
area was slightly decreased with increasing residence time since the time was
generated using a PFA 1/8 inch tubing instead of a continued mixing. The
droplets are generated in the mixer after which they start coalescing in the PFA
tubing, which actually reduces the interfacial area over time. Furthermore, one
can see from \ref{fig:residence time influence} that the extraction of nickel
reaches equilibrium almost instantly.

\Ref{fig:temperature influence} depicts the influence of temperature on the
selectivity on the separation of Co and Ni in the CFRs with a residence time of
60 s and an O/A ratio of 1.5. Raising the temperature until 65
$^{\circ}\mathrm{C}$, the extraction rates increased but dropped significantly
when the temperature went above 65 $^{\circ}\mathrm{C}$. This is ascribed to the
degeneration of the extractant \cite{Devi1998}.

\begin{figure}[H]%effect of temperature, XRD and LS
\centering
 \includegraphics[width = 0.95\textwidth]{Temperature-selectivity.png}
 \caption{Influence of the temperature on the selectivity
 in a single stage setup for the vortex mixer
 (n=3, $Q = 22 ml/min$, $[Co_{start}]= 37~g/l$ and $[Ni_{start}]= 9~g/l$).}
 \label{fig:temperature influence}
\end{figure}

\subsection{Settling time and performance}

The key aspect of CFR is the high surface-to-volume ratio which increases the
heat transfer and reduces the diffusional distances compared to macro-
scale approaches. Another aspect is the excellent controllability of the
interfacial area. This allows for a more predictable phase separation, allowing
for the optimization of the required settling volume. All the prior experiments
have been done using a 25 ml settler volume, which can successfully separate
both phases with small fractions of entrainment. Two designated zones can be
distinguished in the settler. The first zone facilitates droplet coalescence by
wall-liquid interaction. This zone has a width of 0.3 mm with a length of 1 cm
over the full height (5 cm). This forces droplets to adhere and coalesce by
shear. The second zone is wider (1-2 mm), facilitating gravitational phase
separation. By alternating both zones, the mixer is capable of separating the
two phases in a smaller volume than commonly found in classical horizontal
settlers. The phase separation rate is further expedited by the more uniform
droplet sizes generated by both mixers, mainly by the lack of satellite droplets
generated by a Rayleigh-Taylor dispersion in a T-mixer or at the blade tip of
the spindle in case of batch mixing.

\begin{figure}[H]
 \centering
 % Requires \usepackage{graphicx}
 \includegraphics[width=0.95\textwidth]{Settler-evaluatie.png}
 \caption{Impact of the settler volume on water entrainment in the organic
 phase (with and without spiral, the remaining parameters are kept constant
 using the vortex mixer). ($Q = 22 ml/min$)}
 \label{fig:aqueous entrainment}
\end{figure}
%kzit hier

To evaluate the performance of the settling performance, several settlers were
built with the same geometry, while reducing the volume (each time a set of zone
1 and zone 2 repeating unit was removed to reduce the volume) from 25 to 8 ml.
The total flow rate was 22 ml/min with an O/A of 1.5 at 55 $^{\circ}\mathrm{C}$.
Since the residence time was generated with a 1/8 inch PFA spiral, which already
gave some droplet coalescence, the settler performance was evaluated with and
without this residence time spiral. To quantify the settler performance the
weight of entrained water in the organic phase was determined (\ref{fig:aqueous entrainment}).
When using the PFA spiral, the entrained water was less than 1
\%, even at very low settler volumes using the vortex mixer. This cannot be said
for the efficiency of the settler without residence time spiral. This shows that
although the settler is effective in separating both phases in a limited volume
when having the PFA spiral it is not capable of separating the smallest
droplets. The ratio of the volume of the settler compared to that of the mixer +
PFA spiral was approximately 2. This results in a ratio of equilibrating
residence time versus phase separation time equal to 1. The entrainment
difference between a T-mixer and the vortex mixer is more pronounced under
similar conditions, while the entrainment in the T-mixer is 9-10 \% and the
entrainment in the vortex mixer is less than 1 \%.

\subsection{Multistage counter-current}

From the previous results in a single stage setup it is interesting to see how
this setup would hold up against a multistage setup. To achieve this, several
mixer-settlers were coupled in a similar fashion as illustrated in
\ref{fig:reactor setup}. The setup was started in a co-current setup to prime it
and was changed to a counter-current setup to run the experimental evaluation.

% TODO-DONE: figuur Multistage ratio efficiency aanpassen naar log schaal

\begin{figure}[H]%effect of temperature, XRD and LS
\centering
 \subfloat[]{
 \includegraphics[width = 0.45\textwidth]{Multistage-Ratio-sel-effect.png}
 }
 \label{fig:Ratio-selectivity multistage}
 \subfloat[]{
 \includegraphics[width=0.45\textwidth]{multistage-Ratio-efficiency-effect.png}
 \label{fig:temp-E.Ni}
 }
 \caption{Influence of ratio on \textbf{(a)} selectivity and \textbf{(b)}
 extraction efficiency in the multistage setup using the vortex mixer at a
 total flow rate of 22 ml/min with a stage residence time of 60 s.
 ($Q = 22 ml/min$, $[Co_{start}]= 37~g/l$ and $[Ni_{start}]=~9 g/l$ )}
 \label{fig:multistage ratio}
\end{figure}

This configuration was used to screen the influence of O/A ratio in a multistage
setup. The single stage and batch results are included in the figures as
reference. \ref{fig:multistage ratio} depicts the selectivity and the extraction
efficiency for the triple stage setup, batch (15 min residence time) and the
single stage setup. The overall flow rate was kept constant at 22 ml/min with
1/8 inch PFA spirals to accommodate a residence time of 60 s. The $E_{Ni}$ value
decreases slightly for all setups (\ref{fig:multistage ratio}). The extraction
efficiency of Co is on the other hand increasing with increasing O/A ratio. When
the molar ratio of CX272/Co is 2, all the Co will be extracted in the triple
stage. When the molar ratio is further increased, the amount of extracted Ni is
increased resulting in a lowered selectivity. The combination of a vortex mixer
and a PFA spiral with a settler of sufficient size as proposed here shows that a
multistage setup can be used both as a rapid screening tool and as a small-scale
production equipment for multistage solvent extraction.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Conclusion}\label{sec:conclusion5}

Between lab-scale shake tests for chemical screening and industrial exploitation
of solvent extraction it is important to evaluate a multistage setup on a small
scale. This is commonly performed in small mixer-settlers with a total volume of
several liters taking up a serious footprint and requiring a large amount of solvent.
The usable volume and time in such setups is determined by the ratio of settler
(phase separation) and mixer (equilibrating) volume/time. Classically this ratio
ranges from 3 to 10, depending on the chemical and physical properties of the
two liquids.

In this chapter a combination of a CFR and an in-house developed settler was built
and evaluated for rapid screening purposes or as small-scale production of a
multi-stage setup with minimal equipment. The total volume of the setup is 0.5 l
(triple stage), which was tested at a total flow rate of 2.2 l/h for the
separation of Co/Ni. By utilizing this setup, the settler expedites the
separation rate through its function as a coalescer/settler. The ratio of the
available equilibrating time to phase separation is around 1, leading to a total
flow through time of 15 min at 20 ml/min. The optimal parameters could be
screened in a rapid pace on a a square meter. When comparing this setup to
conventional mixer-settlers with a typical equilibration to phase separation
ratio of 4 to 8, the mesoflow setup is more efficient, especially for parameter
screening.

Compared to classical techniques such as centrifugal and mixer-settler
extractors, the setup is more difficult to scale. To circumvent this problem, a
flow distributor could be used for each stage, which increases throughput using
a numbering up technique.

A droplet generator for multiphase systems was defined in \ref{ch:chapter3}
where specific droplet size and size distribution was of key importance. To
further pave the road for future applications this chapter evaluated the use of
several mixers from \ref{ch:chapter4} for the purification/concentration of
metals in an aqueous environment. Now that an increase in purification and
concentration is illustrated using mesoflow reactors for high throughput
equilibrium process screening, the continuous recovery of metals by means of
crystallization is studied in \ref{ch:chapter6}.

%********************************** % Reference Section **********************
\setcounter{NAT@ctr}{0}
\bibliographystyle{unsrtnatV2}
\bibliography{References/libraryV2}
