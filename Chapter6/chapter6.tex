\chapter{Mesoflow reactors: Particle synthesis intensification}\label{ch:chapter6}
\vfill
\begin{flushright}
This chapter is published in Microporous and Mesoporous Materials, vol 226,
2016,Pages 133-139 \\
\textbf{A continuous mesoflow reactor setup as a tool for rapid synthesis of
 micron sized 4A zeolite}\\
Vandermeersch T., T. R.C. Van Assche, J. F. Denayer, W. De Malsche
\end{flushright}
\clearpage
%\begin{flushright}

The recovery of metals from solute state is one of the last steps in hydro-
metallurgic processing. The purified and concentrated metals needs to be
recovered in the desirable form (often solid). One example is that of
crystallization, building structured crystal raster. In this chapter a study of
slow crystallization of inorganic material is studied where the desired phase is
not the finite state of the crystallization due to instability of the product at
elevated temperatures in the mother liquor. Slow crystallization kinetics and a
limited thermodynamical stability of the target crystal phase are characteristic
to zeolite formation, representing some of the key obstructions for fast zeolite
synthesis. The possibility of accelerating NaA zeolite synthesis in a continuous
mesoflow reactor (CFR) is studied. The CFR reduces the thermal lag by increasing
surface-to-volume ratio, expediting heat transfer and mass transfer. The
properties of the CFR and the reference batch synthesized particles were similar
as confirmed by X-ray diffraction, scanning electron microscopy, particle size
measurement using laser scattering and water adsorption equilibria. The reduced
residence time and reduction in thermal lag provided an ideal synthesis
environment for NaA zeolite, without side products, yielding 160 g/h per liter
reactor volume of dry NaA crystals synthesized and 2-3 $\mu m$ particles for a
single CFR. In comparison, the batch process produces 33 g/h per liter reactor
volume The effects of diluting with NaOH-solution and temperature were studied
in the CFR, allowing to determine the optimal conditions. With the enhanced
reaction kinetics gained from the increased temperature and molar composition,
NaA synthesis is performed 10 times faster than in the optimal batch synthesis.
The optimal conditions for the synthesis of NaA in the CFR were determined as: a
gel composition of $Na_2O: 4.75$ - $SiO_2: 1.93 $ - $Al_2O_3: 1.0 $ - $H_2O:
192$, at a synthesis temperature of 150 $^{\circ}\mathrm{C}$ during 16 min
without, aging of the gel mixture. This chapter shows that the bottlenecks in
NaA zeolite synthesis can be widened, resulting in faster synthesis in a CFR,
making it a feasible pathway for more controllable zeolite synthesis at higher
mass production rates (160 g/(h l-reactor)) while reducing the risk of blockage
in a continuous mesoflow reactor.

%\end{flushright}
\pagebreak

%*******************************************************************************
%****************************** Fifth Chapter *********************************
%*******************************************************************************

% **************************** Define Graphics Path **************************
\ifpdf
 \graphicspath{{Chapter6/Figs/Raster/}{Chapter6/Figs/PDF/}{Chapter6/Figs/}}
\else
 \graphicspath{{Chapter6/Figs/Vector/}{Chapter6/Figs/}}
\fi
%\nomenclature[Z]{CFR}{Continuous mesoflow reactor}
%\nomenclature[Z]{CSTR}{Continuous stirred tank reactor}
%********************************** %Nomenclature ******************************

\section{Introduction to particle synthesis in mesoflow reactors}

The previous chapters focused on metal purification and separation in mesoflow
reactors in hydro-metallurgic processes. The current chapter will move away from
the steps of hydro-metallurgic processing and will focus on the recovery step of
the metals, although under form of slow crystallizations rather than metal
precipitations. The previous step resulted in pure dissolved metals, which have
to be recovered from the liquid phase. This is often done by means of
precipitation under the influence of hydroxide formation when working with the
metals. The precipitation of hydrometals is relatively fast and cannot be
controlled other than reaction containment. Besides the uncontrollable and one
directional reaction, sometimes a pure crystalline form is required as final
state. This process is called crystallization. To explore the usage of microflow
reactors this chapter works on the crystallization of zeolites.

%paragraaf inleiding waarom voor zeolieten gekozen

Zeolites are a group of aluminosilicate materials, exhibiting a porous crystal
structure with well-defined structural features as cages and pores. Zeolitic
structures are micro-porous, thus having pores with a diameter of less than $<$
2 nm. Due to these properties, zeolites can be used as molecular sieve. Zeolite
NaA, also known as zeolite 4A, is a material having the Linde type A
(LTA) topology with a Si/Al ratio of 1 and sodium cations counterbalancing the
negatively charged aluminosilicate framework. Zeolites are meta-stable
materials, their crystal structure being strongly dependent on system parameters
and residence time during synthesis \cite{Cundy2005, Brar2001, Cejka2007,
Petrov2012}. For example, zeolite 4A converges towards sodalite when
treated for longer periods of time \cite{Ding2010a}. Besides, the synthesis gel
homogeneity is crucial during the synthesis. Even small differences in
concentration can lead to a different zeolite structure, such as the faujasite
(FAU) topology with a Si/Al ratio of 1.5-2.3 (NaX). The synthesis of zeolites is
strictly bound to system parameters and homogeneity, even when using the correct
stoichiometric quantities \cite{Zhang2013b,Liu2013e}.

Typical synthesis of zeolites is carried out in batch processes \cite{Brar2001,
Zeolita2013, Lechert1998, Ojha2004, Sathupunya2003, Zaarour2014, Liu2013e,
Khan2010, Malgras2015a}. In recent years the interest in continuous chemical
production processes has grown, in the form of continuously stirred tanks in
series (CSTR), tubular mesoflow reactors and capillary mesoflow reactors (CCFR).
The two latter reactor types have led to zeolite synthesis yielding more
homogeneous particles in size, shape and performance, however mostly lacking
satisfactory throughput ($\mu l. min^{-1}$) \cite{Yu2013, Ju2006a}. These types
of continuous mesoflow reactors have been studied extensively for other
applications mainly utilizing their high surface-to-volume ratio, yielding high
heat and mass transfer rates \cite{Burns2001, Kreutzer2005,
commenge_methodology_2011-1, Al-Rawashdeh2012a, Vandermeersch2015, Capretto2011,
Holvey2011, Batdyga1997}. These reactors furthermore exhibit a narrow residence
time distribution and plug flow behavior, in contrast with CSTRs
\cite{Levenspiel1972}. These properties make the continuous mesoflow reactor a
valuable tool for critical processes such as zeolite synthesis. However, the
small channel geometries of capillary mesoflow reactors ($<$ 1 mm) lead an
increased risk of catastrophic reactor blockage destroying the reactor
\cite{Kashid2009, Tonomura2008, Su2014, Liu2015, Khan2004}.

The main advantage of the CCFR \cite{Nguyen, NorbertKockmann2008, Ju2006a,
Biswas2014a} is its fast heat transfer, resulting in a quasi instant heating of
the gel mixture to the desired temperature, in contrast to batch processes that
require a significant time before the gel mixture reaches the desired
temperature. This rapid temperature increase causes the crystals to grow
instantaneously rather than the classical discrete shift from nucleation to
nucleation-growth and finally growth dominated synthesis. Thus far, the only way
to reach similar heating ramps in the entire liquid in batch synthesis is the
use of microwaves, which works well to rapidly increase the temperature but
lacks the ability to chill the liquid in a similar fashion
\cite{Tompsett2006,Brar2001,Cundy2005} and concomitantly stop the reaction after
a specific time interval.

Ju et al. have shown that synthesis in a CCFR is feasible for the production of
NaA nano particles (300 - 900 nm) within 10 min for flow rates of 0.20 up to
0.85 ml.$min^{−1}$ in a stainless steel tube with a 0.7 mm inner diameter. To
achieve a successful synthesis, these authors had to age/cure the gel mixture
prior to the hydrothermal synthesis in the capillary to avoid blockage. This
aging process allows for gel equilibration and selective nucleation at room
temperature \cite{Brar2001}. An alternative to aging is the use of segmented
flow, dividing plugs of synthesis gel with a continuous immiscible phase
\cite{Yu2013}. This significantly reduces the chance of blockage and increases
the homogeneity due to Taylor vortices in the segments inducing self-mixing of
the gel mixture. This technique allows for a fast controlled synthesis of
zeolites but requires a secondary step to remove the immiscible phase and
induces the risk of particle interactions at the liquid-liquid interphase.
Another drawback of CCFR is the low throughput ($\mu l$/min).

NaA zeolite synthesis in a continuous mesoflow reactor (CFR, mm-scale) was
studied, aiming at increasing the overall throughput without losing the ability
of rapid heat transfer of the entire bulk. The reactor setup was studied for the
rapid synthesis of micron sized NaA zeolite crystals at an increased throughput
(ml/min for a single reactor) without aging, to synthesize micron sized NaA
particles under varying conditions. Doing so, the effect of synthesis conditions
(temperature and synthesis mixture composition) has been evaluated to screen the
potential of the CFR. To evaluate the performance of the reactor, the quality of
the obtained zeolite particles was evaluated using X-ray diffraction (XRD),
scanning electron imaging (SEM) and particle size measurements (PSD). The water
adsorption capacity of the CFR synthesized particles was measured, allowing a
more quantitative insight regarding the yield of the synthesis and the
performance gain using a CFR.

\section{Material and methods}

The synthesis gel was prepared according to Thompson et al \cite{Thompson1982}.
The molar ratio of the mixture was ranged in 1:1.93:(3.17-6.33):(128-256)
$Al_2O_3$:$SiO_2$:$Na_2O$:$H_2O$ with a fixed ratio of 40 $H_2O$:$Na_2O$.
(Further referred to as the dilution factor described in
\% described in \ref{tab:dilution table}).

\begin{figure}[H]%effect of temperature, XRD and LS
\centering
 \includegraphics[width = 0.70\textwidth]{fig1-experimental_setup_sketch.png}
 \label{fig:setup}
 \caption{Flow chart of the CFR setup using SS 1/4 inch tubing with 12 ml internal
 volume per meter.}
 \label{fig:flow reactor setup}
\end{figure}



%insert table with molar ratio and dilution factor
\begin{table}[h]
\centering
\caption{Molar ratio for intended dilution factor.}
\begin{tabular}{ccccc}
\hline
Dilution (\%) & $Na_2O$ & $H_2O$ & $SiO_2$ & $Al_2O_3$ \\ \hline
0 & 3.17 & 128 & 1.93 & 1.0 \\
25 & 3.96 & 160 & 1.93 & 1.0 \\
50 & 4.75 & 192 & 1.93 & 1.0 \\
75 & 5.54 & 224 & 1.93 & 1.0 \\
100 & 6.33 & 256 & 1.93 & 1.0 \\
200 & 9.50 & 384 & 1.93 & 1.0 \\ \hline
\end{tabular}
\label{tab:dilution table}
\end{table}

The gel solution was fed to a stainless steel (SS 1/4 inch) reaction tube with a
constant-flow pump via perfluoroalkoxy alkane tubing (PFA tubing, 1/4 inch)
(\ref{fig:flow reactor setup}). The SS tubing, with a length of 12 m, was
submerged in the thermostat with circulating oil (Lauda, Germany, thermal oil
graded to 250 C), which was set to the required temperature. Another SS coil was
submerged in cold water to cool the fluid/gel to room temperature prior to
sampling. The entire reactor was kept at a constant pressure of 5 bar using a
pressurized air cylinder. The fluid was cyclically evacuated from the reactor by
opening a ball valve at specific time intervals to allow fluid to escape the
reactor but keeping the pressure constant without stopping the flow. The flow
rate was 11 or 18 ml/min, which corresponds to a residence time of 16 and 7 min
respectively, with a pressure drop of 0.5-0.35 bar and autogenic pressure of the
gel mixture.

Between each experiment, the reactor was cleaned with deionized water and 10 M
NaOH solution prior to feeding a new gel solution, in order to prevent
contamination or seeding from the previous experiment in the reactor. The solid
particles were recovered from the samples withdrawn from the reactor by
filtration over a nylon membrane (Whattman nylon filter 0.45 um pore size, GE
healthcare life sciences, Diegem, Belgium), followed by washing with deionized
water until the conductivity (Portavo 907 Multi pH, Knick, Germany) was below
1200 $\mu$S/$cm^2$ and finally dried at 90 C for 24 h. The obtained particles
and their quality were characterized with X-ray diffraction (XRD) and scanning
electron micrography (SEM).

The XRD data were collected on a xpert-pro diffractometer (PANalytical, Almelo,
the Netherlands) using Cu K$\alpha$ (1.5405980 \AA) radiation at 50 kV and 30
mA. The intensities were analyzed in the range: 5-50 [2$\theta$] with 0.04 step-
size. The surface and particle shapes were observed by scanning electron
microscopy (SEM-EDX, JEOL, JSM 6400) using an acceleration voltage of 28 kV.
Particle size measurements were done using a laser particle sizer (master sizer
3000, Malvern) with water as medium in which the dried particles were dispersed
using ultrasound (US) for 10 min to break soft agglomerates before measurement.
The particle size distribution was fitted with a Poisson distribution
($\frac{\lambda^k}{k!} e^{-\lambda}$ with $k \in \mathbb{N}$) in which $\lambda$
is the mean and variance of the distribution. An alternative to crystallinity
from XRD data (peak area integration) is that of an adsorption measurement,
which gives a more quantitative measurement of the zeolite quality.

The samples were dried at 180 $^{\circ}\mathrm{C}$ for 3 hours after which they
were activated for 23 hours at 350 $ ^{\circ} C$. The glass vials were capped at
130 $^{\circ}\mathrm{C}$ minimizing atmospheric water in the vial. A mixture of
7 wt\% ultrapure water (Synergy UV, Millipore) and ethanol (99.9 \% pure
analytical grade, VWR, Leuven, Belgium) was added to the dried NaA sample at
room temperature and equilibrated for 1 hour prior to sampling the liquid
fraction with a syringe filter (whattman anotop 10 syringe filter LC, 0.2 um, GE
healthcare life sciences, Diegem, Belgium). The liquid sample was separated
using gas chromatography (GC A7820, Agilent) with a solgel-wax capillary column
(solgel-wax GC capillary 30 m length, 0.25 mm ID, SGE) at 110
$^{\circ}\mathrm{C}$ and detected with a thermal conductivity detector (TCD)
returning the water content in the sample. This allowed for the quantification
of the equilibrium capacity of the sample, which is another correlation with the
conversion, the crystallite form following the procedure in
\cite{VanAssche2011}.

\begin{figure}[H]%correlations of all parameters
\centering
 \includegraphics[width = 0.65\textwidth]{Appendices/Appendix1-GC-equilibration_curve.png}
 \label{fig:gc equilibrium}
 \caption{GC equilibration curve for correlation peak area versus water concentration.}
\end{figure}

\section{Results and discussion}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The synthesis of zeolites is defined by thermodynamics, kinetics and
hydrodynamics with a limited window in which the desired zeolite can be formed.
Since an increase in temperature yields a faster reaction rate, this was the
first parameter to optimize in the CFR to expedite the synthesis process. A
temperature increase from 100 $ ^{\circ}$C (reference) to 150 $ ^{\circ}$C was
applied. Knowing that the activation energy ($E_a$) for NaA crystallization is
60 kJ/mol, the increase in temperature from 100 $^{\circ}\mathrm{C}$ to 150
$^{\circ}\mathrm{C}$ should yield a 10 times faster synthesis (see
\ref{fig:reaction rate increase}) \cite{Zeolita2013}. In contrast, higher
temperatures increase the solubility, reducing crystal growth.

\begin{figure}[H]%effect of temperature, XRD and LS
\centering
 \includegraphics[width = 0.95\textwidth]{fig2-Growth_rate_T_influence.png}
 \caption{The reaction rate increase relative to the reaction rate at 100
 $^{\circ}\mathrm{C}$ as a function of temperature with an activation energy
 ($E_a$) of 60 kJ/mol).
 }
 \label{fig:reaction rate increase}
\end{figure}

A temperature profile in an unstirred batch reactor using a SS casing, PTFE
liner and thermal capacity and conductivity approximated by that of water using
COMSOL 4.3b is shown in \ref{fig:temperature profile}\textbf{(a)}. Several
points are indicated along the axial symmetry (ax symm) showing the temperature
evolution over time for these points. The difference in heating time between
these points is significant when this time is a relatively large fraction of the
total synthesis time. A similar simulation is done for the synthesis of the same
gel mixture with a plug flow regime in a $\frac{1}{4}$ inch SS tubular reactor
at a flow rate of 11 ml/min (see \ref{fig:temperature profile}\textbf{(b)}). The
use of a stirred autoclave would improve the temperature profile, but it would
still be limited by the thermal conductivity of the PTFE liner and SS casing
(see \ref{fig:temperature profile}\textbf{(a)}). It would still require
excessively long heating times, being several orders of magnitude larger than
for the CFR. When the liner and casing would be infinitely thin, achieving
similar heating times would imply that the dimension of the autoclave in the
direction of the temperature gradient would be reduced considerably with a
concomitant reduction of the reactor volume. This would be unfavorable in terms
of throughput compared to the CFR.

\begin{figure}[H]%correlations of all parameters
\centering
 \subfloat[]{
 \includegraphics[width = 0.45\textwidth]{fig3a-temp_profile_batch.png}
 }
 \subfloat[]{
 \includegraphics[width = 0.45\textwidth]{fig3b-tempProfile_plug.png}
 }
 \caption{Temperature profiles for \textbf{(a)} an unstirred batch of 80 ml
 heated to 150 $^{\circ}\mathrm{C}$ and \textbf{(b)} a plug flow setup at 11
 ml/min heated to 150 $^{\circ}\mathrm{C}$ with the dashed line being
 150 $^{\circ}\mathrm{C}$ and 148 $^{\circ}\mathrm{C}$ as tolerance boundary.}
 \label{fig:temperature profile}
 %TODO-DONE ADD AN TOLERANCE TO THE BATCH REACTOR
\end{figure}

When increasing the temperature, the rate at which the liquid is heated becomes
critical. The required time has to be reduced since the solubility decreases
with increasing temperature, resulting in a decrease in the amount of nuclei.
The required time to reach the desired temperature at the applied flow rates and
reactor diameter was 68 seconds in the CFR, whilst the same increase in
temperature would require 1 to 2 hours in an unstirred autoclave. Furthermore,
the CFR behaves like a plug mesoflow reactor, yielding a nearly identical
residence time for each compartment of the gel. The effect of synthesis
temperatures on crystallinity using the CFR with 7 min synthesis time is
illustrated in the XRD-patterns in \ref{fig:XRD temperature influence CFR}. The
SEM results of the synthesis in the CFR are shown in \ref{fig:SEM temperature
influence}. NaA crystals only form when doing the synthesis at 150
$^{\circ}\mathrm{C}$, at lower temperatures only amorphous material is formed.

\begin{figure}[H]%effect of temperature, XRD and LS
\centering
 \includegraphics[width = 0.95\textwidth]{fig4-Letters_Temperature_effect.png}
 \caption{XRD graph showing the influence of synthesis temperature in the CFR
 and a molar ratio
 1:1.92:4.75:192 ($Al_2O_3$:$SiO_2$:$Na_2O$:$H_2O$)
 (temperatures: 100 $^{\circ}\mathrm{C}$ (A), 120 $^{\circ}\mathrm{C}$ (B),
 150 $^{\circ}\mathrm{C}$ (C)).
 }
 \label{fig:XRD temperature influence CFR}
\end{figure}

\begin{figure}[H]%effect temperature SEM
 \centering
 \subfloat[]{
 % Requires \usepackage{graphicx}
 \includegraphics[width = 0.45\textwidth]{fig5a-rsm0229-37(4).png}
 \label{fig:SEM flow 1.5 diluted 100}
 }%rsm0229-37(4).tif 0.06
 \subfloat[]{
 % Requires \usepackage{graphicx}
 \includegraphics[width = 0.45\textwidth]{fig5b-RSM0229-35(3).png}
 \label{fig:SEM flow 1.5 diluted 150}
 }%RSM 35/55 rsm0229-35(2).tif 0.013

 \caption{SEM images showing the influence of temperature in the CFR 50
 \% diluted and processed for 7 min Temperatures: \textbf{(a)} 100
 $^{\circ}\mathrm{C}$ and at \textbf{(b)} 150 $^{\circ}\mathrm{C}$.}
 \label{fig:SEM temperature influence}
\end{figure}

By using the CFR setup, the synthesis gel can be heated to 150 $ ^{\circ}$C in
68 seconds and cooled down to 25 $ ^{\circ}$C in the same time, stopping the
synthesis almost immediately, yielding maximal productivity without impurities.
An overview of the successful synthesis results is given in \ref{tab: summary
table} with resulting mean particle size ($\lambda_{poisson}$) derived from a
Poisson fit on the particle size distribution measured with light scattering
(Malvern mastersizer 3000) and the relative water adsorption capacity. The NaA
sample synthesized in the CFR, processed for 16 min at 150 C, has a water
adsorption capacity, which is 83 \% of that of a fully crystalline NaA sample,
whilst the sample processed under the same conditions for only 7 min shows a
water adsorption capacity of 70 \% of that of a fully crystalline NaA sample. In
comparison with the synthesis in batch, for a similar gel composition, the
adsorption capacity is at the same level (80-85 \%).

%TODO-DONE: incorporate the summary table into the results
\begin{table*}[h]
\centering
\caption{Summarizing table of synthesis methods that yielded NaA crystals,
showing $\lambda_{poisson}$ (the dimensional parameter of the Poisson fit.) and the
relative water adsorption capacity. *As compared to the reference batch
synthesis of 4 h at 100 $^{\circ}\mathrm{C}$ in an autoclave.}
\begin{tabular}{lllllll}
\hline
temp & time & dil. & $\lambda_{poisson}$ & Relative water adsorption capacity*& type \\
C & min & \% & $\mu m$ & frac. & & \\ \hline
100 & 240 & 0 & 3.4 & 1 & batch \\
100 & 240 & 50 & 3.76 & 0.86 & batch \\
150 & 7 & 50 & 4.4 & 0.68 & CFR \\
150 & 16 & 50 & 2.7 & 0.83 & CFR \\ \hline
\end{tabular}
\label{tab: summary table}
\end{table*}


%insert SEM and LS data

When comparing the effect of the residence time and ratio $H_2O:Al_2O_3$
influence shown in \ref{fig:batchversusflow} in combination with the particle
size distribution in \ref{fig:SEM images of batch versus flow} the influence of
residence time and water content shows two distinct differences. When looking at
the batch synthesis of the reference system (1:128 $Al_2O_3:H_2O$) and comparing
it to that of the diluted (1:192 $Al_2O_3:H_2O$) batch treated at 100
$^{\circ}\mathrm{C}$, a significant change in morphology can be seen. By
increasing the water content of the gel, the gel starts to segment if not
stirred, resulting in large concentration gradients. From XRD results and
morphology of the crystals shown in the diluted SEM image the resulting crystals
appear to be NaX crystals.

\begin{figure}[H]%correlations of all parameters
\centering
 \includegraphics[width = 0.95\textwidth]{fig6-PS_LettersBatchversusFlow05diluted.png}
 \caption{Particle size distribution from the particle size measurement for
 batch reference
 (4 hours at 100 $^{\circ}\mathrm{C}$) \textbf{(A)},
 batch diluted with 50 \% water
 \textbf{(B)} (4 hours at 100 $^{\circ}\mathrm{C}$),
 the mesoflow reactor at 150 $^{\circ}\mathrm{C}$ diluted with 50 \% water 7
 min residence time \textbf{(C)} and the mesoflow reactor at 150 $^{\circ}\mathrm{C}$
 diluted with 50 \% water 16 min residence time \textbf{(D)}.
 }\label{fig:batchversusflow}
\end{figure}

The synthesis of the same undiluted gel synthesized at 100 $^{\circ}\mathrm{C}$
in the CFR for 16 min yielded no crystalline material (see \ref{fig:SEM
temperature influence}\textbf{(a)}). Only when increasing the $H_2O:Al_2O_3$
ratio to 192:1 and keeping the temperature at 150 $^{\circ}\mathrm{C}$ clear
crystals were formed. The increase in residence time from 7 to 16 min in the CFR
resulted in more discrete crystals. When looking at the SEM images, the
counterintuitive decrease in apparent particle size with synthesis time seems to
be related to clustering at lower residence times. This might be explained by
the decrease of the surface-to-volume ratio as the crystals grow.

\begin{figure}[H]
 \centering
 \subfloat[]{
 \includegraphics[width = 0.23\textwidth]{fig7a-batch_undiluted.png}
 \label{fig:sem undiluted batch}
 }
 \subfloat[]{
 \includegraphics[width = 0.23\textwidth]{fig7b-batch_diluted.png}
 \label{fig:sem diluted batch}
 }
 \subfloat[]{
 \includegraphics[width = 0.23\textwidth]{fig7c-flow_7min.png}
 \label{fig:sem flow 7 min}
 }
 \subfloat[]{
 \includegraphics[width = 0.23\textwidth]{fig7d-flow_16min2.png}
 \label{fig:sem flow 16 min}
 }
 \caption{SEM images shown for
 \textbf{(a)} batch reference (4 hours at 100 $^{\circ}\mathrm{C}$),
 \textbf{(b)} batch diluted with 50 \% water (4 hours at 100 $^{\circ}\mathrm{C}$),
 \textbf{(c)} the mesoflow reactor at 150 $^{\circ}\mathrm{C}$ diluted with 50
 \% water 7 min residence time and
 \textbf{(d)} the mesoflow reactor at 150 $^{\circ}\mathrm{C}$ diluted with 50
 \% water 16 min residence time.}\label{fig:SEM images of batch versus flow}
\end{figure}

When coming back to the synthesis rate increase due to temperature increase to
150 $^{\circ}\mathrm{C}$, it was shown that it was about 10 times faster than a
similar synthesis in batch at 100 $^{\circ}\mathrm{C}$. When coming from the
reference synthesis, which is stated to take 3 to 4 hours, the synthesis time in
the CFR should take 18-22 min for full synthesis for 100 \% yield.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The synthesis in the CFR yielded 92 wt\% of the expected dry weight (10.3 g/ 80
ml undiluted gel mixture) at 16 min residence time and 150 $^{\circ}\mathrm{C}$
at the optimal gel composition. This is conform the increase in reaction rate
increase due to increased temperature (92 \% of 18 min = 16 min). This would
mean the CFR is capable of synthesizing 160 $g_{dry.NaA}$/(h.$l_{reactor}$) at a flow
rate of 11 ml/min and a pressure drop of 0.5 bar. This is a 5x higher production
rate than a batch reactor, which operates at 32 $g_{dry.NaA}$/(h.$l_{reactor}$). This
leads to two applications of the CFR:

\begin{enumerate}
 \item the use of the CFR as a screening tool, allowing for rapid temperature
 changes keeping nuclei intact and in fact increasing growth rate, vastly
 reducing the chance of side products.
 \item The use of the CFR as a reactor for continuous production of NaA
 crystals at a higher rate than an autoclave.
\end{enumerate}

As an example, the CFR was used to rapidly screen the effect of gel composition,
temperature and residence times on the yield. The obtained materials were
characterized via their water adsorption capacity, as shown in
\ref{fig:combined_effects}. When specifically looking at the results from the
CFR synthesis at 7 min residence time at 150 $^{\circ}\mathrm{C}$, a sharp
increase in adsorption capacity is shown with increasing gel dilution, reaching
an equilibrium around 75 \% adsorption capacity compared to the reference in
batch. When increasing the residence time to 16 min at 50 \% dilution, the
adsorption capacity is increased to 83 \%. The increase in yield/time coincides
with the calculated growth rate gain due to the increase in synthesis
temperature (see \ref{fig:reaction rate increase}). When increasing the
temperature in the batch however, no NaA is formed due to the long time required
to heat and cool the gel versus the actual time required for synthesis at a
given temperature. This increased temperature and lack of temperature control in
batch resulted in the formation of sodalite rather than NaA, which is not
capable of adsorbing water.

\begin{figure}[H]%correlations of all parameters
\centering
 \includegraphics[width = 0.95\textwidth]{fig8-Correlation_capacity_dilution.png}
 \caption{Correlation between the water adsorption capacity (g/g) and the
 different dilution factors and temperatures for the batch and
 CFR relative to the batch synthesis at 100 $^{\circ}\mathrm{C}$.
 A reference to molar composition as function of dilution is available in
 \ref{tab:dilution table}.}
 \label{fig:combined_effects}
\end{figure}

% TODO-DONE : insert ternary diagram

\begin{figure}[H]%correlations of all parameters
\centering
 \includegraphics[width = 0.65\textwidth]{ternair_diagram.png}
 \caption{Ternary diagram for the composition range in which the 4A zeolite is
 formed with \ce{Na2O}, \ce{SiO2}, \ce{Al2O3} ratio.}
 \label{fig:ternary diagram}
\end{figure}

The results from \ref{fig:combined_effects} coincide with the results found via
the simplex method by Garcia-Soto et al. showing that the optimal molar ratio
for the synthesis of NaA is that of 50 \% dilution \cite{Zeolita2013}
(\ref{fig:ternary diagram}). Besides the composition optimization, the influence
of temperature can be depicted. With increasing temperature, the batch
synthesized 4A zeolite quantity decreases significantly due to a conversion to
SOD. In the CFR, the synthesis at limited residence time increases with
temperature but reaches similar adsorption results as a batch synthesized 4A (4
hours) in only 16 min.


In prior studies on the synthesis of NaA zeolite segmentation of particle
fractions and clear solvent was observed at 100 $^{\circ}\mathrm{C}$ in a
capillary mesoflow reactor (CCFR) \cite{Ju2006a, Khan2004}. Although this could
not be observed in the current study due to the pressure control setup acting as
a spray outlet blurring previous states such as segmentation. Nonetheless a
similar segmentation could be expected in this reactor. By increasing the
fraction of water, segmentation within the liquid could be enhanced by reducing
the viscosity of the liquid phase, resulting in a more homogeneous plug flow
behavior of distinct gel/crystal clusters. By doing so, the gel mixture could
exhibit Taylor vortices, resulting in a continuous mixing of the gel, allowing
for homogeneous heat and mass transfer and thus resulting in more homogeneous
particle sizes and properties \cite{Ju2006a}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Conclusion}\label{sec:conclusion}

A pilot scale continuous mesoflow reactor (CFR) was proposed, built and utilized
for the rapid synthesis of NaA zeolite crystals from sodium meta silicate and
sodium aluminate in a NaOH solution. The CFR has a high surface-to-volume ratio,
resulting in fast temperature changes in the liquid during heating and cooling
whilst also showing plug flow behavior. The temperature of the gel mixture can
be increased from 25 $^{\circ}\mathrm{C}$ to 150 $^{\circ}\mathrm{C}$ in only 68
seconds; after crystal formation, the mixture can be efficiently cooled down to
room temperature in the same time, avoiding formation of secondary phases. This
allows for increased crystal growth rates with a faster, controllable crystal
synthesis as a result. The same rate of liquid heating/cooling cannot be
achieved in a classical, unstirred autoclave, which results in sodalite
formation when applying higher temperatures. To overcome this problem, the
liquid could be stirred and heated using microwaves, but even in the case of
using microwave heating, the reaction cannot be quenched fast enough such that
the risk of formation of other meta stable phases still exists. The CFR allows
producing 160 $g_{dry.NaA}$/h per reactor volume. As a reference, an autoclave
produces 33-60 $g_{dry.NaA}$/h per reactor volume at the optimal operating
condition.

The increase reaction rate due to the temperature (following the Arrhenius rate)
increase coincides with the increased mass yield of NaA. This shows that the
synthesis rate of zeolites can be raised by increasing temperature as long as
the reaction can be heated and quenched fast enough to remain within the
synthesis window of the desired zeolite. Under- or overstepping this boundary
would result in alternative zeolites.

The CFR concept can also be employed in the rapid screening and optimization of
synthesis conditions. As an example, the effect of $H_2O$ : $Al_2O_3$ molar
ratio in the gel was evaluated at 150 $^{\circ}\mathrm{C}$, with a constant
$Na_2O$ : $H_2O$ ratio of 40. The results show that the optimal composition for
the synthesis of NaA is similar to that of simulations in a prior study. This
shows that the proposed CFR setup could bridge the gap towards rapid zeolite
synthesis on pilot scale in a limited time frame, by increasing the overall
temperature utilizing its higher temperature ramp rates (both heating and
cooling) to increase crystal growth.

%terugkoppelen naar metaal recovery

This chapter has illustrated the fact the precise process control can expedite
the process throughput as well as result a more homogeneous output with reduced
risk. It is not unreasonable to consider the use of such mesoflow reactor setup
in hydro-metallurgic processes when a precise particle size or characteristic
property has to be acquired for quality reasons. The mesoflow reactor has a
smaller internal volume than an autoclave when requiring similar output per
hour.

%********************************** % Fourth Section *************************************


%********************************** % Reference Section *************************************
\setcounter{NAT@ctr}{0}
\bibliographystyle{unsrtnatV2}
\bibliography{References/libraryV2}
