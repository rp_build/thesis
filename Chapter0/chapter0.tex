%\chapter{Preface} %Title of the First Chapter
%\vfill
%\begin{flushright}
%test bottom right flush text
%\end{flushright}
%\pagebreak

%*******************************************************************************
%*********************************** First Chapter *****************************
%*******************************************************************************
\ifpdf
 \graphicspath{{Chapter0/Figs/Raster/}{Chapter0/Figs/PDF/}{Chapter0/Figs/}}
\else
 \graphicspath{{Chapter0/Figs/Vector/}{Chapter0/Figs/}}
\fi

%********************************** %Nomenclature ***************************************
%\preamble

\section*{Scope of the thesis}\label{sec:scope}
\addcontentsline{toc}{section}{Scope of the thesis}

%********************************** %First Section
%**************************************
Mesoflow reactors are interesting reactors with geometric features below 1 cm,
they allow for precise process control in terms of heat and mass transfer.
Although they are well studied for organic reactions, little literature is
available on inorganic reactions such as hydro-metallurgical ones.\\

Hydro-metallurgical applications are often comprised of a leaching step, which
in turn is followed by purification/concentration and a metal recovery step. The
processes are mostly performed in bulk reactors, resulting in a large residence
time window and a lack of precise process control. To circumvent some of these
problems, this thesis treats both the purification/concentration and recovery
step of these processes by means of process intensification.

Process intensification governs a paradigm shift in process design. It is an
engineering expression referring to the changes made, which render a
manufacturing or a process design substantially improved, enhancing the design
in terms of energy- and cost effectiveness in the broadest sense. One way to
achieve this is by miniaturization, which increases e.g. the driving force. The
usefulness of such miniaturized reactors and designs in industrial applications
using mesoflow reactor technology is studied in this thesis. Mesoflow reactor
technology concerns the manipulation of fluids (gas or liquid) in channels with
dimensions between 1 and several mm. Thanks to these small channel dimensions,
operations like mixing, reactions, dosing, analyzes, etc. have acquired
substantial gains. However, one aspect remains underdeveloped: general
techniques that enable continuous purification. A mesoflow reactor design was
developed for droplet generation based on bifurcating flow distribution, which
can generate homogeneous droplets in an open channel. This design was evaluated
and optimized for multiphase flow, resulting in a small droplet size
distribution, opening the door for precise droplet chemistry.

Solvent extraction involves distribution of one or more chemical species between
two immiscible liquids due to a difference in solubility and is used to separate
the solutes from one another. To execute solvent extraction the liquids are
first contacted, typically by mixing (i.e. dispersing) to acquire equilibrium,
and then phase separated again. Both steps need to proceed as fast as possible.
Equilibrium can be quickly obtained upon intensive mixing, but this adversely
affects the subsequent phase separation step. \\ In this thesis a mesoflow
reactor design that can be employed on pilot scale levels is realized and
evaluated. This mesoflow reactor allows for a fast dispersion of the two
liquids, increasing the interfacial area and enhancing the mass transfer. By
optimizing and characterizing the impact of geometrical features of the mesoflow
reactor on the droplet size distribution, it is possible to obtain a delicate
balance between maximal interfacial surface area to expedite the extraction
process and the maximal droplet size for enhanced phase separations. This was
further explored for the industrial application (Co/Ni separation) in a
multistage counter-current mesoflow reactor setup.

A second technique for which mesoflow reactors could show promising results is
the recovery by crystal synthesis. The difficulty to control the crystal
synthesis process in conventional systems, such as batch processes, make flow
reactors a good solution to increase the inhomogeneity of temperature and
matter in the reactor. In a mesoflow reactor the surface-to-volume ratios can be
a lot higher than in conventional systems, yielding better heat transfer from
external sources, so that the applied energy and temperature can be controlled
much better. Besides faster heat transfer, the diffusion distance in the liquid
is vastly reduced allowing faster mass transport in the fluid ensuring better
homogeneity. \\ The mesoflow reactor setup that was evaluated and developed in
this thesis can be employed in the small production scale for the synthesis of
slow growing crystals at elevated temperature and reduced time frames whilst
remaining within final product specifications. Several hurdles related to
clogging, safety, pressure control and quenching of the crystallization process
have been overcome for a successful synthesis. \\


\pagebreak
\section*{Thesis outline}\label{sec:outline}
\addcontentsline{toc}{section}{Thesis outline}
%********************************** %Second Section ************************
\begin{figure}[H]
 \centering
 % Requires \usepackage{graphicx}
 \includegraphics[width=0.65\textwidth]{doctoraat-diagram.png}
 \label{fig:scope of the project}
\end{figure}%

An introduction to process intensification by means of microfluidics is provided
in ~\textbf{Chapter 1}. First, the general concept of process intensification is
briefly highlighted. Next the usage of mesoflow reactors within this domain is
explored while paying attention to mixing and multiphase liquid-liquid
interaction. Several dimensionless numbers are described to characterize the
mixing behavior. Furthermore, a brief overview of current technological advances
within the probed field is given.

Some contemporary fabrication techniques for mesoflow reactors are treated in
\textbf{Chapter 2}. The technique used in this thesis is computer numerical
control (CNC) assisted micro-milling. This allows for rapid prototyping of
specific reactor development. The technique is discussed with its perks and
cons. The chapter concludes with the more optimized settings for the processing
of polymers and soft metals using micro-milling.

One type of mesoflow device is conceptualized and characterized in
\textbf{Chapter 3}. It is a multi-array flow distributor tested for the
utilization of uniform parallel droplet generation in an open channel. The flow
distributors have a fractal design and are intertwined to maximize the droplet
generating outlets per utilized surface area with minimal droplet size
distribution.

Several mixing principles are evaluated in \textbf{Chapter 4} for solvent
extraction. The utilized mixers following this chapter are characterized with
fluorescence dilution experiments in a two phase mixing experiment. The tested
mesoflow mixers are a T-mixer, a split-and-recombine mixer and a inhouse
developed vortex mixer, but are not limited to these. These mixers differ with
respect to effective mixing properties from lamination to (chaotic) advection.
The chapter concludes with an assessment of the performance impact of L-L
extraction in a single component extraction and the influence of energy
dissipation as a step for mixer optimization for the use in the hydro-
metallurgical purification/concentration step.

These mixers are then used together with an inhouse developed settler for a
multistage counter-current mesoflow setup for the purification/separation of
metals (Co/Ni) from an aqueous solution with cyanex 272, this is described in
\textbf{Chapter 5}. Besides single stage extraction, the setup was expanded to a
mesoflow reactor setup in a multistage counter-current setup with self-
regulating phase separators.

The recovery by means of crystallization is subsequently studied in mesoflow
reactors in \textbf{Chapter 6} and focuses on the high surface-to-volume ratio
of mesoflow reactors for the rapid synthesis and process control. A setup was
built allowing for precisely controlled continuous crystal synthesis with
pressure regulation.

The thesis concludes with ~\textbf{Chapter 7}, providing a summary of the
results for the utilization of mesoflow reactors towards industrial applications
in hydrometallurgy and tries to provide the reader with insight in applications
and operation ranges where the mesoflow reactors show potential.
