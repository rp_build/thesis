0..7 | % {
    #loop folders
    cmd /c mklink /j "diff\Chapter$_\Figs" "current\Chapter$_\Figs"
}
cmd /c mklink /j "diff\Preamble" "current\Preamble"
cmd /c mklink /j "diff\Figs" "current\Figs"
cmd /c mklink /j "diff\References" "current\References"
cmd /c mklink /j "diff\Publications" "current\Publications"
cmd /c mklink /j "diff\Dedication" "current\Dedication"
cmd /c mklink /j "diff\Classes" "current\Classes"
cmd /c mklink /j "diff\Acknowledgement" "current\Acknowledgement"
cmd /c mklink /j "diff\Abstract" "current\Abstract"

cmd /c mklink "diff\thesis-info.tex" "current\thesis-info.tex"