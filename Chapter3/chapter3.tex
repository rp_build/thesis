\chapter{Flow distribution: Parallel and identical droplet generation}
\label{ch:chapter3}
\vfill
\begin{flushright}
\textbf{Part of this chapter is submitted as patent [EP14173333.7]}
\end{flushright}
\clearpage

Droplet size control and uniformity is one of the prerequisites for the study
of droplet interactions and particle synthesis. Studies have already been
conducted using a single T-junction or a flow-focusing device. These devices
show single droplet characteristics but lack high throughput and the ability to
study droplet-droplet interaction. Flow parallelization is a pathway often
looked towards when searching for increased capacity of mesoflow reactors.
Several studies have been performed towards solutions for this problem. Both in
2D and 3D, flow distributors have been based upon either the lattice or fractal
design. For this thesis, two fractal distributors have been intertwined
showing the possibilities of flow distributors as a tool to allow
\textit{en-mass} droplet generation with a narrow droplet size distribution.
The intertwined flow distributors end in a combined outlet nozzle with a
T-junction but can be envisioned with a flow focusing nozzle as well. The
research limited itself to the study of T-junction types, which have a more
controllable droplet generation and a higher tolerance for flow irregularities
with limited impact on the size distribution. In the end, the distributor was
able to generate droplets with a coefficient of variance below 9 \% in an open
channel proving the concept of the submitted patent. This opens the gate for
droplet generation with a limited consequences of clogging a nozzle,
maintaining similar properties as a regular T-junction of identical size.

\pagebreak

%*******************************************************************************
%****************************** Third Chapter **********************************
%*******************************************************************************

% **************************** Define Graphics Path **************************
\ifpdf
 \graphicspath{{Chapter3/Figs/Raster/}{Chapter3/Figs/PDF/}{Chapter3/Figs/}}
\else
 \graphicspath{{Chapter3/Figs/Vector/}{Chapter3/Figs/}}
\fi

%********************************** %Nomenclature ****************************
% first letter Z is for Acronyms
% first letter A is for Roman symbols
% first letter G is for Greek Symbols
% first letter X is for Other Symbols
% first letter R is for superscripts
% first letter S is for subscripts
%\nomenclature[G]{$\rho$}{density ($\frac{kg}{m^3}$)}
%********************************** %Nomenclature ***************************

\section{Introduction to single phase flow distribution}

Droplet size control and uniformity can be one of the prerequisites for the
study of droplet interactions and particle synthesis. Microreactors are one of
the possible tools to acquire these properties. They are used to specifically
control flow characteristics, enhancing the overall mass and heat transfer at
specific locations in the reactors. Numbering up of microfluidic devices is one
of the key steps that has to be taken to move from the lab to the small
production scale. With this numbering up, a direct drawback of increased
utility/equipment with increasing numbers increases the cost significantly.
Numerous attempts have been undertaken to provide viable solutions
\cite{Lepercq-Bost2008, Cubaud2012a, Podgorska2005, Ban2000}. Each with it is
own pro's and cons making a trade-off between holdup volume and distribution
uniformity. Scaling of microreactors is another step that is undertaken to
avoid numbering up to some extent. The challenge is to provide similar
properties to the reactor whilst increasing the throughput. Some commercial
companies producing microreactors have already proven that scaling of
microreactors can be done if it is assured that the specific characteristics of
the microreactors are reasonably similar (Alfalaval, Ehrfeld, IMM). They mean
that the mixing intensity and transfer rates (both, energy and mass transfer)
should remain constant per produced volume throughout the scaling phases.

In contrast, it must be emphasized that some specific flow patterns are hard to
achieve in such scaling scheme. Such example can be found in multiphase droplet
generation in which the specific interaction between solid-liquid-liquid is
dominant in micro-channels and so far cannot be approached by the conventional
scaling laws without jeopardizing critical properties such as droplet size
uniformity or local flow velocity. To retain the specific dimension per channel
in a microreactor and increasing the throughput, numbering up of these channels
is the only option when all channels require an identical mass and heat
transfer rate. To achieve this, the flow has to be distributed evenly.

One type of distributor in the 2D field found in literature has been described
by \citet{Desmet1997} disregarding the holdup volume of the flow distributor
(\ref{fig:gert desmet}).

\begin{figure}[H]
\centering
\subfloat[]{
	\includegraphics[width=0.3\textwidth]{gert_desmet.png}
	\label{fig:gert desmet}
}
\subfloat[]{
	\includegraphics[width=0.3\textwidth]{2D-bifurcating.png}
	\label{fig:2D distributor}
}\\
\subfloat[]{
	\includegraphics[width=0.3\textwidth]{3D-distributor.png}
	\label{fig:3D distributor}
}
\subfloat[]{
	\includegraphics[width=0.3\textwidth]{lattice.png}
	\label{fig:lattice}
}
\caption{Several examples of flow distributors: \textbf{(a)} showing a
2D flow distributor with parallel connections used for 2D HPLC chips
\cite{vangelooven_computer_2010};\textbf{(b)} a bifurcating flow
distributor \cite{Liu2013a};\textbf{(c)} a 3D bifurcating flow distributor
 \cite{Luo2005};\textbf{(d)} a lattice structure for flow parallelization
 \cite{commenge_methodology_2011-1}.}
\label{fig: flow distributors}
\end{figure}

% gert_desmet.png \cite{vangelooven_computer_2010-1}
% 2D-bifurcating.png \cite{Liu2013a}
% 3D-distributor.png \cite{Luo2005}
% lattice.png \cite{commenge_methodology_2011-1}
%TODO-DONE include illustrative graphics of 2D, 3D, ramified, slate and showerhead

They studied the need for parallel and joining flow paths concerning flow
uniformity and the prevention of large changes due to single channel blockage.

Besides this structural approach, similar to the lattice structured flow
distributors (\ref{fig:lattice}), a slate feed has been used. This is an open
flow field narrowing as we move further from the initial feed connection thus
ensuring uniform flow pattern at each sequential channel along the feed slate
\cite{amador_flow_2004}. This slate design (\ref{fig:slate design}) has the
advantage of its robustness but has a relatively large volume compared to the
actual reaction channels.

A third type is bifurcating flow distributors (\ref{fig:2D distributor}), which
is a type derived from nature \cite{Tondeur1998}. It has a minimal volume but a
limited robustness and blockage of a single channel will have a significant
impact on the remaining channels.

Moving to 3D flow distribution the last type of the 2D flow distributors can be
extended to the $3^{rd}$ dimension, which increases the outlet density per
surface area compared to 2D flow distributors (\ref{fig:3D distributor})
\cite{Tondeur2011c, Luo2005, Luo2007a, Tondeur2011a, Tondeur2004,
Tondeur2011a,Vladisavljevic2008}. \citet{Tondeur2004} calculated and
experimentally validated the use of this type of flow distributor.
\citet{Liu2013a} optimized the design of this type of flow distributors,
reducing the flow variance governing in a bifurcating junction by means of CFD
calculations and optimization.

All these designs are very useful for single flow distribution but only the
slate design allows for high throughput of multiphase systems
\cite{al-rawashdeh_design_2012, Vladisavljevic2008}. \citet{Luo2005} have
defined several guidelines concerning such type of fractal flow distributors
for single phase flow doing chain like calculations with the pressure loss as
cost for minimal volume while maintaining even flow distribution. They
concluded that the channel cross section per stage should drop by $1/3^{rd}$ by
each occurring stage
\cite{Luo2005}. \citet{Luo2005} have defined several guidelines concerning such
type of fractal flow distributors for single phase flow doing chain like
calculations with the pressure loss as cost for minimal volume while maintaining
even flow distribution. They concluded that the channel cross section per stage
should drop by 1/3 by each occurring stage. This design type is
useful when thinking of utilizing monolithic catalyst blocks with mesoporous
channels.

A lot of research is put into segmented flow processes in microfluidic devices.
The reason for this is the highly controlled formation of the droplets and
bubbles, resulting in a predictive behavior for the experiment.

\begin{figure}
\centering
\subfloat[]{
\includegraphics[width=0.35\textwidth]{T_junction.png}
\label{fig:T-junction}
}
\subfloat[]{
\includegraphics[width=0.35\textwidth]{FF.png}
\label{fig:FF}
}
\caption{Illustration of a T-junction droplet generator \textbf{(a)}
\cite{Fu2009} and a flow focusing droplet generator \textbf{(b)} \cite{Wang2015a}.}
\label{fig:droplet generators}
\end{figure}

% FF.png \cite{Wang2015}
% T_junction.png \cite{Fu2009a}
% TODO-DONE: include figure for segmented flow and a T-junction and a flow focusing
% device

The most frequently used device is a regular T-junction (\ref{fig:T-junction})
and a flow focusing (\ref{fig:FF}) device \cite{Takeuchi2005, Zhao2011b,
Cubaud2012a}. The advantage of a T-junction is the ease of fabrication. Prior
research provided a deep inside into the way a droplet is formed over a wide
range of flow rates and T-junction geometries \cite{Yeom2011, Kumar2011,
Zaccone2007}.

\begin{equation}\label{eq:Capillary number}
Ca = \frac{\mu V}{\gamma}
\end{equation}

When working at low capillary numbers (Ca) (under $ 10^{-2}$)
\citet{Garstecki2006} found that the ratio of length and width of the droplet
is defined by the flow rate ratio and the physico-chemical properties of the liquid
phases and the reactor material ($\alpha$). This relation can be expressed as:

\begin{equation}\label{eq:ratio droplet width length}
L/w = 1 + \alpha Q_cQ_d^{-1}
\end{equation}

with L the length of the slug, w the channel width and Q the flow rates of
the continuous and dispersed phase.

Because the Ca-number is low, the shear stresses exerted on the
interface of the emerging droplet are not sufficient to distort it.
Consequently the droplet blocks the entire cross section of the main channel.
Doing so, the pressure upstream of the droplets is increased and leads to
squeezing of the droplet neck. This squeezing proceeds at the rate
proportional with the flow of the carrier fluid after which the neck breaks
and a droplets is formed. In this regime the droplet diameter is directly
controlled by the flow rate ratio. Above a Ca-number of $10^{-2}$, the
shear stress on the interface causes ripples and a dripping effect
\cite{Garstecki2006}. If the Ca-number is further increased, the
regime shifts from dripping to jetting flow (similar to a fosset leak).

\begin{figure}[H]
 \centering
 \includegraphics[width=0.65\textwidth]{Lwratio_Flowratio.png}
 \caption{The droplet size with shifting flow rate ratio. With L the droplet length
 and w the channel width of the T-junction from which the ratio is dependent
 on the flow rate ratio of continuous (oil) and dispersed
 (water) phase \cite{Garstecki2006}.}
 \label{fig:LwRatio_Flowratio}
\end{figure}

At low flow rate ratios the change in droplet geometry is limited by the channel
size (\ref{fig:LwRatio_Flowratio}). With increasing flow rate ratios (over 0.3),
the droplet length over channel width will increase in a linear fashion with
this flow rate ratio. The shift from dripping to jetting is also present when
using flow focusing devices
\cite{Garstecki2005}.

The main drawback of a T-junction or flow focusing device as droplet generator
is the fact that it is hard to scale and retain the droplet geometry due the
required characteristics described above. Instead of scaling these type of
reactors, one must turn towards numbering-up of the reactor with parallel
channels. This was successfully done by using processes where the droplets are
retained in there specific channel \cite{Kobayashi2011a}. An alternative is to
inject the droplets in an open geometry. This allows the droplets to reform to
spherical shapes rather than the elliptic forms and possible behave as a free-
flowing droplet.

In this chapter a 2.5D fractal flow distributor approach has been applied to
increase the outlet density per unit of surface. The developed fractal
distributor design consists of two individual 3D bifurcating flow distributors,
which are intertwined and combined at the last branch where they form parallel
T-junctions. The design was validated using optimal imaging of droplet formation
towards an open channel.


\section{Experimental methods}

The experimental setup consists of two stainless steel pressurized vessels
used to inject the fluid into the system at pressures up to two bar under
controlled conditions (\ref{fig:image-setup}). This pressure was
correlated with the flow rate for the given liquid in the vessel. The reactors
were designed and built in-house as described in \ref{ch: material and methods}.

\begin{figure}[H]
 \centering
 \includegraphics[width=0.45\textwidth]{image-setup.png}

 \caption{Setup of the experimental setup for droplet generation in the flow
 distributors. Two pressure vessels of 1 l each, with ball-valves connected
 to the chip.}
 \label{fig:image-setup}

 %AANVULLEN
\end{figure}


\begin{figure}[H]
	\centering
		\includegraphics[width=0.70\textwidth]{Flowcell-design.PNG}
	\caption{The flow cell used to study the droplets directly after the nozzle
	 outlets and further combined into a shallow and wide channel (2 cm wide 1
	 mm deep) to study the general generated droplets.}
	\label{fig:Flowcell-Design}
\end{figure}

The experiments were performed using milliQ water with technical grade, iso-
propanol (IPA, Sigma Aldrich) and sodium dodecyl sulphate micelles (SDS, Sigma
Aldrich) as continuous phase whilst the dispersed phase was filtered corn oil
with a viscosity of 35 mPa.s (at 20 $^{\circ}\mathrm{C}$) and a density of 780
$kg/m^3$. The droplet generator is attached to a flow cell that guides the flow
from the surface of the droplet generator to a 2 mm deep channel and the same
width so the overall droplet size distribution can be measured (\ref{fig:Flowcell-Design}).
This droplet size is then used to determine the distribution
efficiency of the droplet generator.

\section{Design definition and boundaries}

%what are the requirements

To achieve a high throughput of generic identical droplets that can be
controlled in a precise manner, the flow velocity at each T-junction should be
identical. Therefore the flow has to be distributed from the initial single feed
to the final T-junctions with minimal disturbances and irregularities. The
design has to be a balance between flow uniformity and pressure drop. This idea
leads to several viable options such as the planar flow distributor in a dual
layer where each outlet is connected perpendicularly to the continuous flow
distributor, as e.g. mostly used in fuel cells. Although this option is well
known and used in several studies \cite{Al-Rawashdeh2012b, jithesh_effect_2012},
this solution has a limited volumetric throughput, especially when one is
interested in freely moving droplets without contacting the walls because of
non-homogeneous effect (heterogeneous nucleation in crystallization, adhesion to
the wall, droplet breakup or coalescence). An alternative path is that of a
strictly defined and constructed 3D bifurcating flow distributor in which the
flow paths are defined by a bifurcation at each step down. In \ref{fig:design} a
generic view is shown.

\begin{figure}[H]
	\centering
\subfloat[]{
 	\includegraphics[width = 0.45\linewidth]{SideView.png} %TODO-DONE figuur aangeven)
 \label{fig: Frontview Distributor}
 }
\subfloat[]{
	\includegraphics[width = 0.45\linewidth]{TopView.png}
 \label{fig: Topview Distributor}
 }\\
\subfloat[]{
\includegraphics[width = 0.45\linewidth]{Foto-Distributor.png}
 \label{fig: Foto Distributor}
}

\caption{The conceptual and realized design of the 3D droplet generator
 (channel size 500 $\mu m$).\textbf{(a)} front view of a 3D droplet
 generator \textbf{(b)} top view of a 3D droplet generator,\textbf{(c)} Image of
 the realized transparent 3D droplet generator.}
 \label{fig:design}
\end{figure}

By intertwining two fractal flow distributors and connecting the outlets in a
T-junction, it is possible to control the conditions at each outlet in a
precise manner. These conditions should be similar to the conditions at a single
T-junction as tested at laboratory scale. This could opens the door to increase the
total throughput of droplets per surface without drastically increasing the
total engineering effort required, omitting additional scaling steps.

The fractal droplet generator is built from sequential stages, each containing
the next branch with channel cross-sections $2/3^{rd}$ of the previous branch.
We compromise between flow uniformity when splitting the flow and
the increased pressure drop as well as the internal volume of the fractal
droplet generator \citet{Luo2005}. A second fractal distributor is intertwined,
in the first generator, starting three levels below the first branch.
The nozzle density and spacing is dependent on the final channel
width, the channel width of the branch three levels above the final branch and
the minimal wall thickness. In theory it is possible to continue the dichotomy
to generate additional levels and increase the nozzle density and decrease the
pore size of the final branches until the lower fabrication limit is reached.

To further support the concept, the effect of adding additional levels to the
fractal design was calculated, on the increased pressure drop, the capillary
number (Ca) with $\sigma$ the interfacial tension and u the linear velocity:

\begin{equation}
Ca = \frac{\mu u}{\sigma}
\end{equation}

The pressure drop is described by:

\begin{alignat}{2}\label{eq:pressure drop in rectangular channels}
\Delta p_k &= \frac{8u_kl_k\mu}{\pi r^4_k}\\
\Delta p_k &\cong \frac{Q 12 \nu l}{[1 - 0.630 \frac{h}{w}] h^3w}, \text{ for h} \ll
\end{alignat}

with r the channel hydraulic diameter for cylindrical channels and Q the flow
rate with l, h and w the geometry of a rectangular channel.

\begin{figure}[H]
	\centering
		\includegraphics[width=0.90\textwidth]{Diameter-dependancy.PNG}
	\caption{Dependency of the characteristic pressure drop (circle),
	capillary number (square) on the channel cross section (triangle)
	normalized to the top level values of the bifurcating design.} %explain cleaner
	\label{fig: Pressure drop-Capillary number-branching}
\end{figure}

The results of the analysis in \ref{fig: Pressure drop-Capillary
number-branching} show the added relative pressure drop, the relative
reduction in Ca number and relative reduction cross section to the
first branch (level 0) values.

By adding a final level (assuming more than 5 levels), the pressure drop will
increase with a fraction of the pressure drop relative to the first level. The
Ca number decreases in a similar fashion. This indicates that adding additional
levels to the fractal droplet generator should not impact the flow
characteristics other than the reduced Ca number in the final level and the
reduced flow rate per channel.

\section{Results and discussion}

%% what were the results? How was it made and characterized and what was measured?

To further predict the droplet size and especially the droplet size distribution
in this 3D droplet generator, the flow pattern and velocity field were studied
with computational fluid dynamics (CFD - Comsol 4.3b, settings can be found in
\ref{ch:appendix1}). This study helped to define a minimal spread over the system in
the current configuration. The CFD study indicated that the flow variance,
looking to the velocity at the nozzle outlets, has a coefficient of variance
(COV) of 3.6 \% (\ref{fig:CFD velocity field in a T junction}). This
optimization has been studied in a single distributor by \citet{Liu2013a}.
Because the droplet size distribution (DSD) ranges in a the typical single
mixers is relatively low without optimization (ranging from 2 to 9 \% COV on
droplet sizing, \cite{Moon2014a}), this optimization of channels was not
implemented due to the intertwining network complexity.

\begin{figure}[H]
	\centering
		\includegraphics[width=0.85\textwidth]{VelocityField.PNG}
	\caption{CFD velocity field slice in a T-junction of a bifurcating channel
	showing the velocity inhomogeneity and resulting in a velocity variance at
	the outlets of 3.6 \%.}
	\label{fig:CFD velocity field in a T junction}
\end{figure}

\subsection{Droplet size variance dependency on flow rate and flow ratio}

In \ref{fig:flow ratio dependancy} the experimental results of the
droplets for several flow rate ratios but with an equal total flow rate are shown. The
flow rates of the individual streams were measured (by means of g/min) prior to
the experiments and correlated to the vessel pressure. The mean droplet size and
the span (10 - 90 \% droplet size range) are depicted. The mean droplet size does
not depend on the flow rate ratio within the tested range but the span does
increase below a flow rate ratio of 0.15. This could be attributed to the low
flow rate of the dispersed phase resulting in creeping flow. Since creeping flow
has a tendency to take a random path rather than split evenly in a T-junction,
it will have a much higher chance of generating random droplets. This effect was
also noticeable when doing the experiment and evaluating at the nozzle outlets.

\begin{figure}[H]
	\centering
		\subfloat[]{
\includegraphics[width=0.45\textwidth]{Flow_ratio_dependancy.png}
	\label{fig:flow ratio dependancy}
	}
		\subfloat[]{
\includegraphics[width=0.45\textwidth]{Flow_rate_dependancy.png}
	\label{fig:flow rate dependancy}
	}\\
	\subfloat[]{
		\includegraphics[width=0.45\textwidth]{density.png}
		\label{fig:PSD data}
		}
	\caption{Mean droplet size in function of \textbf{(a)} flow rate ratio and
	\textbf{(b)} overall flow rate. (\~ 50-100 measurements per setting).
	\textbf{(c)} shows a droplet size distribution measurement. A typical
	droplet size distribution of a single experiment (\textbf{(c)}).}
\end{figure}

A change in total flow rate (\ref{fig:flow rate dependancy}) does not influence
the mean droplet size. The effect of flow rate ratio in \ref{fig:flow rate dependancy}
is displayed by the two points at 22 ml/min with their respective span. Even
there, the influence is most pronounced in the span rather than the mean droplet
size.

When fitting with a linear function the influence of neither flow rate or flow rate
ratio are relevant ($10^{-2}x + 380$) indicating the independence and
robustness of the design even when maldistribution for a select number of
channels is present. The average COV measured in these experiments above a
flow ratio of 1\/10 is 9 \%, falling within the accepted uniform distribution.

% TODO-DONE pas tekst aan.
% TODO-No apparent dis-correlation between linear value and influence of
% ratio: correlation study (statistical study) er is afwijking maar dunno hoe

\subsection{Phase separation}

% TODO-DONE: ref naar COV waarde batch

Since the settling time in a laminar flow regime (Re $<$ 2) is proportional to
$d^{-2}$ in phase separation, it is important to reduce the fraction of droplets
smaller than the desired size. Using an impeller or static mixer setup the
droplet size distribution is large \cite{Paul2004}. Typically the COV is more
than 35 \% with a significant fraction of extremely small droplets. This
fraction is accountable for the largest time fraction when performing phase
separation. By using the fractal flow distributor design, these extremely small
droplets can be reduced without influencing the mean droplet size. Furthermore,
the use of this design allows for production of precise droplet size ranges
significantly reducing the extremely large droplets as well.

To verify this aspect, both setups (batch mixer - magnetic stirrer and the
fractal distributor) have been used for the emulsification of oil in water+SDS
for a settling experiment taking images on identical time intervals. The
results of the phase separation from a fractal distributor and
batch mixing are shown in a movie (\textbf{\href{https://youtu.be/5NAWy380uB8}}).

From this result it is clear that the phase separation of the liquids mixed in
batch take much longer to phase separate compared to that of the fractal
distributor, although both have the same mean droplet size at start. The
reason for this is the specific droplet generation regime difference. The
squeezing regime at low Ca-numbers dictates that the droplet size
has a similar behavior as that of the channel. Whilst in batch, the droplet
generation is dependent on the relative velocity difference of the fluids,
which in turn is dependent on the shear rate at the blades of the impeller.
Since this shear rate is related to the rotation speed and the distance from
the rotation axle, the exerted force is not homogeneous in the batch mixer,
resulting in a large variety of droplets. The generated droplets in the batch
reactor are still subject to these forces, resulting in smaller droplets by
impact upon solids or coalescence with other droplets. By using the fractal
distributor, this uncontrollable factor is removed, resulting in strictly
controlled droplet sizes with limited side effects.

\section{Conclusion}

Droplet size uniformity is an important aspect for droplet-droplet interaction
studies as well as the generation of specific droplet sizes itself. To increase
the throughput of a T-junction a combination of fractal flow distributors was
designed, built and evaluated without sacrificing the specific droplet generation
mechanism from a single T-junction. This concept was successfully developed and
evaluated in this chapter and is submitted as patent (EP14173333.7).

This concept extends the fractal distribution for the parallelization of droplet
generation in open tubes allowing for a droplet generation at 20-30 ml/min with
a droplet size coefficient of variance less than 9 \%, whereas less than 10 \%
is accepted as homogeneous droplet sizing and a COV of 25-35 \% is generic for
impeller mixing.

This was achieved by intertwining two fractal distributors ending in a
T-junction structure. By dividing the flow from each path the flow velocity was
reduced. The reduced flow rate at the final T-junction was reduced enough to
guarantee a capillary number below $10^{-2}$. This produced similar droplet
sizes with a significant tolerance for flow maldistribution due the squeezing
regime that was only changing the local droplet generation frequency. The
proposed reactor design represents an important step towards uniform droplet
generation that can be employed in the generation of size-specific spheres and
droplets of a predefined size. The advantage of this method, which is at the
same time a disadvantage, lies in the fact that the channel geometry is the
determining factor of the droplet size at low Ca numbers.

The next chapters will discuss the influence of energy dissipation on the
overall droplet size in several micromixers utilizing vortices and lamination
rather than droplet squeezing regimes, extending the exploration of interfacial
surface generation aimed towards an increased extraction and phase separation
rate without requiring low Ca-numbers.



%********************************** % Fourth Section *************************************
\setcounter{NAT@ctr}{0}
\bibliographystyle{unsrtnatV2}
\bibliography{References/libraryV2}
