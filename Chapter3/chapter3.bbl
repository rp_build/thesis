\begin{thebibliography}{31}
\providecommand{\natexlab}[1]{#1}
\providecommand{\url}[1]{\texttt{#1}}
\expandafter\ifx\csname urlstyle\endcsname\relax
  \providecommand{\doi}[1]{doi: #1}\else
  \providecommand{\doi}{doi: \begingroup \urlstyle{rm}\Url}\fi

\bibitem[Lepercq-Bost et~al.(2008)Lepercq-Bost, Giorgi, Isambert, and
  Arnaud]{Lepercq-Bost2008}
{\'{E}}~Lepercq-Bost, M.-L Giorgi, A~Isambert, and C~Arnaud.
\newblock Use of the capillary number for the prediction of droplet size in
  membrane emulsification.
\newblock \emph{Journal of Membrane Science}, 314\penalty0 (1-2):\penalty0
  76--89, 2008.

\bibitem[Cubaud et~al.(2012)Cubaud, Jose, Darvishi, and Sun]{Cubaud2012a}
T~Cubaud, B.~M Jose, S~Darvishi, and R~Sun.
\newblock Droplet breakup and viscosity-stratified flows in microchannels.
\newblock \emph{International Journal of Multiphase Flow}, 39:\penalty0 29--36,
  2012.

\bibitem[Podg{\'{o}}rska(2005)]{Podgorska2005}
W~Podg{\'{o}}rska.
\newblock Scale-up effects in coalescing dispersions: comparison of
  liquid-liquid systems differing in interface mobility.
\newblock \emph{Chemical Engineering Science}, 60\penalty0 (8-9):\penalty0
  2115--2125, 2005.

\bibitem[Ban et~al.(2000)Ban, Kawaizumi, Nii, and Takahashi]{Ban2000}
T~Ban, F~Kawaizumi, S~Nii, and K~Takahashi.
\newblock Study of drop coalescence behavior for liquid-liquid extraction
  operation.
\newblock \emph{Chemical Engineering Science}, 55\penalty0 (22):\penalty0
  5385--5391, 2000.

\bibitem[Desmet et~al.(1997)Desmet, Verelst, and Baron]{Desmet1997}
G~Desmet, H~Verelst, and G~Baron.
\newblock Transient and stationary axial dispersion in vortex array flows�i.
  axial scan measurements and modeling of transient dispersion effects.
\newblock \emph{Chemical Engineering Science}, 52\penalty0 (14):\penalty0
  2383--2401, 1997.

\bibitem[Vangelooven and Desmet(2010)]{vangelooven_computer_2010}
J~Vangelooven and G~Desmet.
\newblock Computer aided design optimisation of microfluidic flow distributors.
\newblock \emph{Journal of Chromatography A}, 1217\penalty0 (43):\penalty0
  6724--6732, 2010.

\bibitem[Liu and Li(2013)]{Liu2013a}
H~Liu and P~Li.
\newblock Even distribution/dividing of single-phase fluids by symmetric
  bifurcation of flow channels.
\newblock \emph{International Journal of Heat and Fluid Flow}, 40:\penalty0
  165--179, 2013.

\bibitem[Luo and Tondeur(2005)]{Luo2005}
L~Luo and D~Tondeur.
\newblock Optimal distribution of viscous dissipation in a multi-scale branched
  fluid distributor.
\newblock \emph{International Journal of Thermal Sciences}, 44\penalty0
  (12):\penalty0 1131--1141, 2005.

\bibitem[Commenge et~al.(2011)Commenge, Saber, and
  Falk]{commenge_methodology_2011-1}
J.-M Commenge, M~Saber, and L~Falk.
\newblock Methodology for multi-scale design of isothermal laminar flow
  networks.
\newblock \emph{Chemical Engineering Journal}, 173\penalty0 (2):\penalty0
  541--551, 2011.

\bibitem[Amador et~al.(2004)Amador, Gavriilidis, and Angeli]{amador_flow_2004}
C~Amador, A~Gavriilidis, and P~Angeli.
\newblock Flow distribution in different microreactor scale-out geometries and
  the effect of manufacturing tolerances and channel blockage.
\newblock \emph{Chemical Engineering Journal}, 101\penalty0 (1-3):\penalty0
  379--390, 2004.

\bibitem[Bejan and Tondeur(1998)]{Tondeur1998}
A~Bejan and D~Tondeur.
\newblock Equipartition, optimal allocation, and the constructal approach to
  predicting organization in nature.
\newblock \emph{Revue G{\'{e}}n{\'{e}}rale de Thermique}, 37\penalty0
  (3):\penalty0 165--180, 1998.

\bibitem[Tondeur et~al.(2011)Tondeur, Fan, and Luo]{Tondeur2011c}
D~Tondeur, Y~Fan, and L~Luo.
\newblock Flow distribution and pressure drop in 2d meshed channel circuits.
\newblock \emph{Chemical Engineering Science}, 66\penalty0 (1):\penalty0
  15--26, 2011.

\bibitem[Luo et~al.(2007)Luo, Fan, and Tondeur]{Luo2007a}
L~Luo, Y~Fan, and D~Tondeur.
\newblock Heat exchanger: from micro- to multi-scale design optimization.
\newblock \emph{International Journal of Energy Research}, 31\penalty0
  (13):\penalty0 1266--1274, 2007.

\bibitem[Tondeur and Menetrieux(2011)]{Tondeur2011a}
D~Tondeur and C~Menetrieux.
\newblock Channel interlacing: a geometric concept for intensification and
  design of the internal structure of fluid contactors.
\newblock \emph{Chemical Engineering Science}, 66\penalty0 (4):\penalty0
  709--720, 2011.

\bibitem[Tondeur and Luo(2004)]{Tondeur2004}
D~Tondeur and L~Luo.
\newblock Design and scaling laws of ramified fluid distributors by the
  constructal approach.
\newblock \emph{Chemical Engineering Science}, 59\penalty0 (8-9):\penalty0
  1799--1813, 2004.

\bibitem[Vladisavljevi{\'{c}} et~al.(2008)Vladisavljevi{\'{c}}, Kobayashi, and
  Nakajima]{Vladisavljevic2008}
G.~T Vladisavljevi{\'{c}}, I~Kobayashi, and M~Nakajima.
\newblock Generation of highly uniform droplets using asymmetric microchannels
  fabricated on a single crystal silicon plate: effect of emulsifier and oil
  types.
\newblock \emph{Powder Technology}, 183\penalty0 (1):\penalty0 37--45, 2008.

\bibitem[Al-Rawashdeh et~al.(2012{\natexlab{a}})Al-Rawashdeh, Fluitsma,
  Nijhuis, Rebrov, Hessel, and Schouten]{al-rawashdeh_design_2012}
M~Al-Rawashdeh, L~Fluitsma, T~Nijhuis, E~Rebrov, v~Hessel, and J~Schouten.
\newblock Design criteria for a barrier-based gas-liquid flow distributor for
  parallel microchannels.
\newblock \emph{Chemical Engineering Journal}, 181-182:\penalty0 549--556,
  2012{\natexlab{a}}.

\bibitem[Fu et~al.(2009)Fu, Ma, Funfschilling, and Li]{Fu2009}
T~Fu, Y~Ma, D~Funfschilling, and H.~Z Li.
\newblock Bubble formation and breakup mechanism in a microfluidic
  flow-focusing device.
\newblock \emph{Chemical Engineering Science}, 64\penalty0 (10):\penalty0
  2392--2400, 2009.

\bibitem[Wang et~al.(2015)Wang, Zhu, Fu, and Ma]{Wang2015a}
X~Wang, C~Zhu, T~Fu, and Y~Ma.
\newblock Bubble breakup with permanent obstruction in an asymmetric
  microfluidic t-junction.
\newblock \emph{AIChE Journal}, 61\penalty0 (3):\penalty0 1081--1091, 2015.

\bibitem[Takeuchi et~al.(2005)Takeuchi, Garstecki, Weibel, and
  Whitesides]{Takeuchi2005}
S~Takeuchi, P~Garstecki, D.~B Weibel, and G.~M Whitesides.
\newblock An axisymmetric flow-focusing microfluidic device.
\newblock \emph{Advanced Materials}, 17\penalty0 (8):\penalty0 1067--1072,
  2005.

\bibitem[Zhao and Middelberg(2011)]{Zhao2011b}
C.-X Zhao and A.~P Middelberg.
\newblock Two-phase microfluidic flows.
\newblock \emph{Chemical Engineering Science}, 66\penalty0 (7):\penalty0
  1394--1411, 2011.

\bibitem[Yeom and Lee(2011)]{Yeom2011}
S~Yeom and S.~Y Lee.
\newblock Size prediction of drops formed by dripping at a micro t-junction in
  liquid-liquid mixing.
\newblock \emph{Experimental Thermal and Fluid Science}, 35\penalty0
  (2):\penalty0 387--394, 2011.

\bibitem[Kumar et~al.(2011)Kumar, Paraschivoiu, and Nigam]{Kumar2011}
V~Kumar, M~Paraschivoiu, and K~Nigam.
\newblock Single-phase fluid flow and mixing in microchannels.
\newblock \emph{Chemical Engineering Science}, 66\penalty0 (7):\penalty0
  1329--1373, 2011.

\bibitem[Zaccone et~al.(2007)Zaccone, G{\"{a}}bler, Maa{\ss}, Marchisio, and
  Kraume]{Zaccone2007}
A~Zaccone, A~G{\"{a}}bler, S~Maa{\ss}, D~Marchisio, and M~Kraume.
\newblock Drop breakage in liquid-liquid stirred dispersions: modelling of
  single drop breakage.
\newblock \emph{Chemical Engineering Science}, 62\penalty0 (22):\penalty0
  6297--6307, 2007.

\bibitem[Garstecki et~al.(2006)Garstecki, Fuerstman, Stone, and
  Whitesides]{Garstecki2006}
P~Garstecki, M.~J Fuerstman, H.~a Stone, and G.~M Whitesides.
\newblock Formation of droplets and bubbles in a microfluidic
  t-junction-scaling and mechanism of break-up.
\newblock \emph{Lab on a chip}, 6\penalty0 (3):\penalty0 437--446, 2006.

\bibitem[Garstecki(2010)]{Garstecki2005}
P~Garstecki.
\newblock Formation of bubbles and droplets in microfluidic systems.
\newblock In \emph{Transport}, volume~53, pages 183--202. 2010.

\bibitem[Kobayashi et~al.(2011)Kobayashi, Neves, Wada, Uemura, and
  Nakajima]{Kobayashi2011a}
I~Kobayashi, M.~A Neves, Y~Wada, K~Uemura, and M~Nakajima.
\newblock Large microchannel emulsification device for producing monodisperse
  fine droplets.
\newblock \emph{Procedia Food Science}, 1\penalty0 (Icef 11):\penalty0
  109--115, 2011.

\bibitem[Al-Rawashdeh et~al.(2012{\natexlab{b}})Al-Rawashdeh, Yu, Nijhuis,
  Rebrov, Hessel, and Schouten]{Al-Rawashdeh2012b}
M~Al-Rawashdeh, F~Yu, T~Nijhuis, E~Rebrov, v~Hessel, and J~Schouten.
\newblock Numbered-up gas-liquid micro/milli channels reactor with modular flow
  distributor.
\newblock \emph{Chemical Engineering Journal}, 207-208:\penalty0 645--655,
  2012{\natexlab{b}}.

\bibitem[Jithesh et~al.(2012)Jithesh, Bansode, Sundararajan, and
  Das]{jithesh_effect_2012}
P~Jithesh, A~Bansode, T~Sundararajan, and S.~K Das.
\newblock The effect of flow distributors on the liquid water distribution and
  performance of a pem fuel cell.
\newblock \emph{International Journal of Hydrogen Energy}, 37\penalty0
  (22):\penalty0 17158--17171, 2012.

\bibitem[Moon et~al.(2014)Moon, Cheong, and Choi]{Moon2014a}
S.-K Moon, i.~W Cheong, and S.-W Choi.
\newblock Effect of flow rates of the continuous phase on droplet size in
  dripping and jetting regimes in a simple fluidic device for coaxial flow.
\newblock \emph{Colloids and Surfaces A: Physicochemical and Engineering
  Aspects}, 454\penalty0 (1):\penalty0 84--88, 2014.

\bibitem[Paul et~al.(2004)Paul, Atiemo-Obeng, and Kresta]{Paul2004}
E.~L Paul, V.~a Atiemo-Obeng, and S.~M Kresta.
\newblock \emph{Handbook of industrial mixing: science and practics}.
\newblock 2004.

\end{thebibliography}
