SET FILENAME=thesis


del /s *.log

del /s *.bbl

del /s *.aux

del /s *.bbg

del /s *.nls

del /s *.nlo

del /s *.out

del /s *.in
d
del /s *.ilg

del /s *.fls

del /s *.fdb_latexmk

latexmk -xelatex -silent -interaction=nonstopmode  -f -g -jobname="%FILENAME%"  "%FILENAME%.tex"

latexmk -xelatex -silent -interaction=nonstopmode  -f -g -jobname="%FILENAME%"  "%FILENAME%.tex"
makeindex "%FILENAME%.aux"
makeindex "%FILENAME%.idx"

makeindex "%FILENAME%.nlo" -s nomencl.ist -o "%FILENAME%".nls

latexmk -xelatex -silent -interaction=nonstopmode  -f -g -jobname="%FILENAME%"  "%FILENAME%.tex"

makeindex "%FILENAME%.nlo" -s nomencl.ist -o "%FILENAME%".nls

latexmk -xelatex -silent -interaction=nonstopmode  -f -g -jobname="%FILENAME%"  "%FILENAME%.tex"

